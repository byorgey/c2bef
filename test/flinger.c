#include "consttab.h"

DLL const_table;

int main() {
  vs * tempVS = vs_createStandard();
  vs_pos * temp = create_vs_pos();

  vs * outputVS;
  
  init_const_table();

  add_constant(tempVS, 45, 543, 20489);
  add_constant(tempVS, 2, 2, -100000000);
  add_constant(tempVS, 789, 895, 9090909);
  

  outputVS = generate_flinger();

  vs_printScreen(outputVS, 0);

  return 0;
}
