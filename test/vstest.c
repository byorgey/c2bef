#include "vs.h"

int main() {
  int i, j;
  
  vs * vsUL = vs_createStandard();
  vs * vsBL = vs_createStandard();
  vs * vsUR = vs_createStandard();
  vs * vsBR = vs_createStandard();
  vs * tempVS = vsUL;
  vs_pos * temp = create_vs_pos();


  /* link vs screens together */
  vs_setVS(vsUL, vsBL, DOWN);
  vs_setVS(vsUL, vsUR, RIGHT);
  vs_setVS(vsBL, vsBR, RIGHT);
  vs_setVS(vsUR, vsBR, DOWN);

  
  vs_putChar(tempVS, '*', 0, 0);

  set_vs_pos(temp, 9, 4, RIGHT);
  set_vs_pos_boundaries(temp, 4, 12, 0, 19);
  /*  vs_putString(tempVS, "Bash is an sh-compatible command language interpreter that executes commands read from the standard input or from a file.  Bash also incorporates useful features from the Korn and C shells (ksh and csh).", temp, 1);*/
  vs_putString(tempVS, "hi", temp, CONTIGUOUS);
  vs_putString(tempVS, " how ", temp, CONTIGUOUS);
  vs_putString(tempVS, "hi", temp, CONTIGUOUS);
  vs_putString(tempVS, "are youuu", temp, CONTIGUOUS);
  printf("vstest: %d, %d\n", temp -> pos_x, temp -> pos_y);
  vs_putString(tempVS, "doing xyz", temp, CONTIGUOUS);
  vs_putString(tempVS, "today??", temp, CONTIGUOUS);
  vs_putString(tempVS, "++good", temp, CONTIGUOUS);

  

  
  vs_printScreen(vsUL, 1);

  return 0;
}
