#include "vs.h"

int main() {
  vs * tempVS = vs_createStandard();
  vs_pos * temp = create_vs_pos();

  set_vs_pos(temp, 5, 5, LEFT);

  vs_putString(tempVS, "Outputting to the left, negatively... la la la la let's try wrapping this and see what happens.  this should be long enough.", temp, WRAP);

  vs_printLinkedScreens(tempVS -> leftscreen);
  vs_printLinkedScreens(tempVS);
  
  return 0;
}
