# Main makefile for project.

CC = gcc
CCFLAGS = -g

MAKE = gmake

OBJECTS = obj/dll.o obj/hash.o obj/symtab.o obj/fungify.o \
	obj/lex.yy.o obj/y.tab.o obj/syntreeops.o obj/stack.o \
	obj/beflib.o obj/vs.o obj/vs_algo.o obj/befgen.o \
	obj/consttab.o
TARGETS = src/lex.yy.c src/y.tab.c $(OBJECTS)

LIBS = -lm

all : 
	$(MAKE) fungify

obj/lex.yy.o : src/lex.yy.c
	gcc -o obj/lex.yy.o -Iinclude -c src/lex.yy.c

obj/y.tab.o : src/y.tab.c
	gcc -o obj/y.tab.o -Iinclude -c src/y.tab.c

obj/%.o : src/%.c include/%.h
	$(CC) $(CCFLAGS) -Iinclude -c $< -o $@

src/lex.yy.c : include/scan.l include/y.tab.h
	cd include ; lex -o../src/lex.yy.c scan.l 

src/y.tab.c : include/gram.y src/lex.yy.c
	cd include ; yacc -o../src/y.tab.c gram.y 

fungify : $(TARGETS)
	$(CC) $(CCFLAGS) $(LIBS) -o fungify $(OBJECTS)

clean :
	cd obj ; rm -f *.o
	cd src ; rm -f lex.yy.c y.tab.c
	rm -f fungify
