/* stack.h

   Stack ADT, based on doubly-linked list implementation in dll.{h,c}

   C-to-Befunge translation project

   (c) 2003 Steve Winslow and Brent Yorgey 

*/

#ifndef __STACK_H
#define __STACK_H

#include "dll.h"

typedef DLL_node_ptr Stack_node_ptr;
typedef DLL Stack;

/* Stack_Create  O(1)

   Pre:  None
   Post: Memory has been allocated and stack initialized to empty
         Returns a pointer to the newly created stack
	 ALWAYS CALL Stack_Destroy() to clean up a stack allocated
  	   with Stack_Create()! Otherwise you'll have a memory leak. 
*/
Stack Stack_Create ();

/* Stack_Clear  O(n)

   Pre:  parameter stack has been initialized with Stack_Create
   Post: parameter stack points to an empty stack
         free() is called for the item pointers of any nodes
	 returns OK on success, failure otherwise
*/
status_code Stack_Clear ( Stack stack );

/* Stack_Destroy  O(n)

   Pre:  parameter stack has been initialized
   Post: all memory associated with stack has been freed
         Note: stack is NOT set to NULL (it cannot be).
         Note: This is not the same as emptying the stack.  The stack
	   should not be used again before another call to Stack_Create.
	   To make a stack empty, use Stack_Clear.
	 Returns OK on success, failure otherwise
*/
status_code Stack_Destroy ( Stack stack );


/* Stack_IsEmpty  O(1)

   Pre:  stack has been initialized
   Post: returns true iff stack is empty, i.e. has zero elements.
*/
bool Stack_IsEmpty ( const Stack stack );

/* Stack_Size  O(1)

   Pre:  stack has been initialized
   Post: returns the number of elements contained in the stack
*/
long int Stack_Size ( const Stack stack );



/* Stack_Top  O(1)

   Pre:  stack has been initialized
         stack is non-empty
   Post: returns a pointer to the top element of the stack
*/
Item_ptr Stack_Top ( const Stack stack );


/* Stack_Push  O(1)

   Pre:  stack has been initialized
   Post: the given item has been inserted at the end of the stack.
         returns OK on success, otherwise overflow or failure
*/
status_code Stack_Push ( Stack stack, Item_ptr item );


/* Stack_Pop  O(1)

   Pre:  stack has been initialized
         stack is non-empty
   Post: The item at the top of the stack has been popped,
           and the memory associated with it has been freed.
	 Returns OK on success or out_of_bounds if stack is empty
*/
status_code Stack_Pop ( Stack stack );



/* Stack_Traverse   O(n)

   Pre:  ProcessItem is a function which does something to each
           item of the stack (e.g., print).
   Post: ProcessItem has been called on each item of the stack, in order
           from top to bottom.
*/
void Stack_Traverse ( Stack stack, void (*ProcessItem) (Item_ptr item) );



#endif
