/* syntreeops.h

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0av     "3002 )c( thgirypoc">:#,_$@
      >"hgatruM moT morf nelotS"^
*/

#ifndef __SYNTREEOPS_H
#define __SYNTREEOPS_H

#include "syntaxtree.h"

#define REAL_FAST 9999

struct lexval {
  int line;
  char * strptr;          /* text of string literals */
  identdesc * identptr;   /* pointer to identifier descriptor in hash */
  long constval;          /* value of constant */
  int consttype;          /* 1=char, 2=int, 3=float */
};

struct listhead {
  node *head, *tail;
};

typedef union {
  int flag;
  node *treenode;
  struct lexval lexeme;
  struct listhead list;
} YYSTYPE;


extern char *errormess;
extern int curlineno;

#ifdef yaccing
extern int yydebug;
#endif

/* Tree building operations provided by syntreeops.c */

/* Initial set-up of the syntax tree */
void setup_syntaxtree ( node * treeroot );

/* Delete the node passed.  And all of its children too!  Mwahahaha!!! */
void deletetree(node * treeroot);

/* makelist - Add 'newnode' to the (possibly empty) list 'list'.  */
struct listhead makelist(struct listhead list, node *newnode, int line);

/* makesingle  -  Make a node with a one child.    */
node * makesingle(nodetype op, node *child, int line);

/* makedouble  -  Make a node with a two children.    */
node * makedouble(nodetype op, node *child1, node *child2, int line);

/* maketriple  -  Make a node with a three children.    */
node * maketriple(nodetype op, node *child1, node *child2, node *child3, 
		 int line);

/* Create a node representing a memory reference.         */
/* Not doing nifty displacement thingy. */
node * makerefvar(node * baseaddr, dcldesc * var, int line );

/* Create a tree node for a constant value.  For now, a new node is created */
/* for each constant.  Eventually, this may have to be changed for value    */
/* numbering such that a single copy of each consant is created.            */
node * makeconst(int value, int ischar, int line);

/* Create a tree node for an identifier. */
node * makeident(identdesc * varptr, int line);

/* Create a tree node for a string literal. */
node * makestring(char * string, int line);

/* errornode   -  Return an Nerror node */
node * errornode(int line);

/* print out syntax tree */
void printsyntree(node * treeroot, int indent);

/* returns number of children for the given node type */
int getnumberofchildren(nodetype type);

/* prints the name of the given node type. */
void printnameoftype(nodetype type);

/* evaluates a node's subtree's constant expressions, prunes them, and creates a new Nconst node */
void foldconstant (node * treeroot);

/* returns the value of a node and its subtrees */
long determinevalue (node * treeroot);

/* finds the next non-refvar node */
node * findnonrefvar (node * treeroot);

#endif
