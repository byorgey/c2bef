/* beflib.h

   Various befunge-specific code (i.e. Code You Would Never Ever Need
   Anywhere Else)

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#ifndef __BEFLIB_H
#define __BEFLIB_H

/* How long would the string returned by num2befrep be? */
int befreplen ( long int num );

/* Return a string that, when executed left-right in Befunge, will
   result in a value of num being on top of the stack. */
char * num2befrep ( long int num );

#endif
