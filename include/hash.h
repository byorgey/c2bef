/* hash.h
   Generic hash table implementation.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

   ??? MAKE BETTER COMMENTS!!
*/

#ifndef __HASH_H
#define __HASH_H

#define HASH_SIZE 50

#include "dll.h"

typedef struct {
  DLL table[HASH_SIZE];
  long int num_elts;
  
  int (*hash_func)( Item_ptr key );
  bool (*isEqual)( Item_ptr key, Item_ptr item );
} Hash_struct;

typedef Hash_struct * Hash;


/* Hash_Create  O(1)

   Creates a new hash, given a hash function with the signature shown.
   The return value of the hash function will be modded by the hash
   size.

   Always call Hash_Destroy() to clean up a hash allocated with
   Hash_Create.
*/
Hash Hash_Create ( int (*hash_func)( Item_ptr item ), 
		   bool (*isEqual)(Item_ptr key, Item_ptr item) );

/* Hash_Clear  O(n)

   Delete the entire contents of a hash.
*/
status_code Hash_Clear ( Hash h );

/* Hash_Destroy  O(n)

   Destroy a hash (free all memory associated with it).
*/
status_code Hash_Destroy ( Hash h );

/* Hash_Insert  O(1)

   Insert the given item into the hash.
*/
void Hash_Insert ( Hash h, Item_ptr item, Item_ptr key );

/* Hash_Lookup  O(1)

   Return a pointer to the first matching item found, or NULL if no
   matching item was found.
*/
Item_ptr Hash_Lookup ( Hash h, Item_ptr key );

/* Hash_Traverse  O(n)

   Apply ProcessItem to each item in the hash.
*/
void Hash_Traverse ( Hash h, void (*ProcessItem) (Item_ptr item) );

#endif
