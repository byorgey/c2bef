/* vs.h

   Data structure for "virtual screens" of Funge-space.
   (The term "virtual screens" is preserved for
   historical reasons.)

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#ifndef __VS_H
#define __VS_H

#include <stdio.h>

#define VS_WIDTH 50
#define VS_HEIGHT 20

#define NOWRAP 0
#define WRAP 1

typedef enum direction { LEFT, RIGHT, UP, DOWN } direction;

typedef struct virtualscreen {
  char * * space;
  long screen_x, screen_y;
  long global_x, global_y;
  struct virtualscreen * leftscreen;
  struct virtualscreen * rightscreen;
  struct virtualscreen * upscreen;
  struct virtualscreen * downscreen;

/* For dynamic programming algorithm to calculate the size of a
   glom. */
  int maxright, maxdown; 
  int visited;

} vs;

typedef struct virtualscreen_pos {
  int pos_x;
  int pos_y;
  direction dir;
} vs_pos;

/* creates a new virtual screen and initializes all cells to ASCII 32. */
vs * vs_createStandard();
vs * vs_create(int x, int y);
vs * vs_createEmpty();

void vs_destroy(vs * screen);

/* set screen's neighbor screen in direction dir to screen2 */
vs * vs_setVS(vs * screen, vs * screen2, direction dir);

/* get pointer to linked screens */
vs * vs_getVS(vs * screen, direction dir);

/* get pointer to linked screens; create them if they don't exist */
vs * vs_createVS(vs * screen, direction dir);

/* print contents of vs to standard output */
void vs_printScreen(vs * screen, int printBorder);

/* print one line of given funge-space. */
void vs_outputLine(vs * screen, int lineno, FILE * fp);

/* print all screens linked, starting with this one as the upper left. */
void vs_outputLinkedScreens(vs * screen, FILE * fp);

/* print all screens to standard output */
void vs_printLinkedScreens(vs * screen);

/* return a pointer to the vs in which the given cell resides.
   if create is on, create a new vs to hold it. */
vs * vs_findScreen(vs * screen, int * x, int * y, int create, int change_pos);

/* return pointer to actual char cell of this x,y position */
char * vs_findCell(vs * screen, int x, int y, int create);

/* get contents of this cell */
/* return -1 on out-of-bounds error */
char vs_getChar(vs * screen, int x, int y);

/* vs_replaceChar will overwrite the current value of the cell
   with extreme prejudice. */
/* return -1 on out-of-bounds error */
void vs_putChar(vs * screen, char c, int x, int y);

/* put string headed in this direction, starting with given cell */
/* ??? NOTE: wrapping will overwrite on screen edges, regardless of
   overwrite setting */
/* pass 1 to wrap to enable wrapping */
/* pass 1 to overwrite to replace char's instead of checking to see
   if they're already filled */
int vs_putString(vs * screen, char * st, vs_pos * currentPos, int wrap);

/* determine code boundaries in this direction */
int vs_findCodeBoundary(vs * screen, int followLinks, direction dir);



/**************************************
 *  Used by vs internal functions.    *
 *  Don't even think about trying to  *
 *    use these in any other way.     *
 **************************************/

/* return the x-value of the current vs right boundary in "world" coords
   based on the current vs_pos */
int vs_screenRightBoundary(vs * screen, vs_pos * curPosition);

/* return the y-value of the current vs down boundary in "world" coords
   based on the current vs_pos */
int vs_screenDownBoundary(vs * screen, vs_pos * curPosition);



/******************
 *  vs_pos stuff  *
 ******************/

/* create a vs_pos */
vs_pos * create_vs_pos();

/* set value of vs_pos elements */
void set_vs_pos(vs_pos * temp, int x, int y, direction dir);


/*******************
 *  utility funcs  *
 *******************/

/* build a path from the current position to the lower left or
   lower right corner */
/* return -1 if unable to build path without destroying data */
int vs_buildExitPath(vs * screen, vs_pos * curPosition);

/* call putChar with a vs_pos instead of x & y */
void vs_putCharPos(vs * screen, char c, vs_pos * curPosition);

#endif
