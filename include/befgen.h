/* befgen.h

   Header file for main Befunge code generation routines.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#ifndef __BEFGEN_H
#define __BEFGEN_H

#include "dll.h"
#include "syntaxtree.h"
#include "vs.h"

/* Location of the stack pointer relative to the storage offset. */
#define SP "001-"    /* (0, -1) */

/* Location of scratchpad registers relative to the storage offset. */
#define SCR_A "101-" /* (1, -1) */
#define SCR_B "201-" /* (2, -1) */

/* Location of the first global var relative to the storage offset. */
#define GLOBAL_OFFSET_X 3
#define GLOBAL_OFFSET_Y (-1)  

/* Location of the calling offset within the current stack frame. */ 
#define CALL_X "0"
#define CALL_Y "1"

/* Beginning of local variable storage within the current stack frame. */
#define LOCAL_OFFSET 2

/* extern DLL vs_list;  (in befgen.c) */

/* Generate initialization code. For now, DON'T stick it in the vs
   list. 
*/
vs * gen_init_code ();


/* Generate code for an entire syntax tree. Stick the many resulting
   vscreens into the vs list. 
*/
void gen_code ( node * tree_root );


/* Generate code for the function pointed to by func_root, and return
   the resulting vscreen glom.  (where "vscreen glom" is a technical
   term referring to a group of vscreens that are all linked
   together.)
*/
vs * gen_func_code ( dcldesc * funcEntry );

void gen_expr_code ( node * expr, vs * funcvs, vs_pos * pos );

void gen_compound_code ( node * code_root, vs * funcvs, vs_pos * pos );

void gen_stmt_code ( node * stmt, vs * funcvs, vs_pos * pos );

/* Link all the vscreens together into one humongous glom. Return a
   pointer to the upper-leftmost one. 
*/
vs * link_vs ();


/* Write out the final program text. */
void write_glom ( vs * glom, char * filename );


#endif
