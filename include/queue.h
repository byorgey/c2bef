/* queue.h

   Queue ADT, based on doubly-linked list implementation in dll.{h,c}

   C-to-Befunge translation project

   (c) 2003 Steve Winslow and Brent Yorgey 
*/

#ifndef __QUEUE_H
#define __QUEUE_H

#include "dll.h"

typedef DLL_node_ptr Queue_node_ptr;
typedef DLL Queue;

/* Queue_Create  O(1)

   Pre:  None
   Post: Memory has been allocated and queue initialized to empty
         Returns a pointer to the newly created queue
	 ALWAYS CALL Queue_Destroy() to clean up a queue allocated
  	   with Queue_Create()! Otherwise you'll have a memory leak. 
*/
Queue Queue_Create ();

/* Queue_Clear  O(n)

   Pre:  parameter queue has been initialized with Queue_Create
   Post: parameter queue points to an empty queue
         free() is called for the item pointers of any nodes
	 returns OK on success, failure otherwise
*/
status_code Queue_Clear ( Queue queue );

/* Queue_Destroy  O(n)

   Pre:  parameter queue has been initialized
   Post: all memory associated with queue has been freed
         Note: queue is NOT set to NULL (it cannot be).
         Note: This is not the same as emptying the queue.  The queue
	   should not be used again before another call to Queue_Create.
	   To make a queue empty, use Queue_Clear.
	 Returns OK on success, failure otherwise
*/
status_code Queue_Destroy ( Queue queue );


/* Queue_IsEmpty  O(1)

   Pre:  queue has been initialized
   Post: returns true iff queue is empty, i.e. has zero elements.
*/
bool Queue_IsEmpty ( const Queue queue );

/* Queue_Size  O(1)

   Pre:  queue has been initialized
   Post: returns the number of elements contained in the queue
*/
long int Queue_Size ( const Queue queue );



/* Queue_Front  O(1)

   Pre:  queue has been initialized
         queue is non-empty
   Post: returns a pointer to the top element of the queue
*/
Item_ptr Queue_Front ( const Queue queue );


/* Queue_Enqueue  O(1)

   Pre:  queue has been initialized
   Post: the given item has been inserted at the rear of the queue.
         returns OK on success, otherwise overflow or failure
*/
status_code Queue_Enqueue ( Queue queue, Item_ptr item );


/* Queue_Dequeue  O(1)

   Pre:  queue has been initialized
         queue is non-empty
   Post: The item at the front of the queue has been deleted,
           and the memory associated with it has been freed.
	 Returns OK on success or out_of_bounds if queue is empty
*/
status_code Queue_Dequeue ( Queue queue );



/* Queue_Traverse   O(n)

   Pre:  ProcessItem is a function which does something to each
           item of the queue (e.g., print).
   Post: ProcessItem has been called on each item of the queue, in order
           from front to rear.
*/
void Queue_Traverse ( Queue queue, void (*ProcessItem) (Item_ptr item) );


#endif
