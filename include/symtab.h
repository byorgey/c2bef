/* symtab.h

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0av     "3002 )c( thgirypoc">:#,_$@
      >"hgatruM moT morf nelotS"^
*/

#ifndef __SYMTAB_H
#define __SYMTAB_H

#ifndef NULL
#define NULL 0
#endif

#define FALSE 0
#define TRUE 1

#include "hash.h"
#include "stack.h"

#include "vs.h"

/* Can't include syntaxtree.h because of circular include issues. Just
   declaring union nodeunion does the trick, though.
*/
union nodeunion;

/* Structure specification for the nodes that form the identifier   */
/* table component of the symbol table.  There will be one of these */
/* nodes for each distinct identifier encountered in the program.   */
/* They are allocated and initialized by the scanner.               */

typedef struct iddesc {
  char *name;                  /* The characters string form of the identifer          */
  union dcldesc *globaldecl;   /* Pointer for declarations of global-scope vars        */
  union dcldesc *localdecl;    /* Pointer for declarations of current local-scope vars */
} identdesc;

/* The identifier descriptor hash. Declared somewhere else & populated
   from the lexer. */
extern Hash idHash;

/* The hash requires a function for hashing keys and a function for
   comparing a key and an item. Also the printidentdesc_hash function
   can be used to traverse the hash and print out each item.
*/

int stringHash ( void * str_ptr );

bool identdescMatches ( void * str_ptr, void * iddesc_ptr );

void printidentdesc_hash ( void * identdesc_ptr );

/****************************************************************************/
/* The following definitions all describe parts of the 'declaration table'  */
/* component of the symbol table.  There will be one 'declaration           */
/* descriptor' for each declaration in the program.  (Note: Since there may */
/* be several  declarations for a given identifier, there may be several    */
/* declaration descriptors associated with a single identifer descriptor.)  */
/* These nodes are allocated and initialized during semantic processing.    */
/****************************************************************************/

/* Enumeration type used to label the various type of declaration  */
/* descriptors that can occur in the symbol table.                 */
typedef enum {
  funcdecl,        /* function declarations                   */
  vardecl,         /* global and local variables              */
  formaldecl,      /* Formal parameter names                  */
  arraydecl,       /* Type names for array types              */
  structdecl,      /* Type names for struc types              */
  fielddecl,       /* Component names of struct type          */
  pointerdecl,     /* Pointers!  w00t!                        */
  typedecl         /* Types: int, char, long                  */
} decltype;


/* The following are structure specifications for the various types of */
/* declaration descriptors that can occur in the symbol table.         */
/* Certain common components are arranged so that they can be accessed */
/* using the structure type 'unkdesc' regardless of the actual type of */
/* the declaration descriptor.                                         */

/* The type used to store the name associated with a machine  */
/* instruction is accessed through the define CODELBL so that */
/* students can easily redefine this type as appropriate.     */
#ifndef CODELBL
#define CODELBL char
#endif

/* All declaration descriptors contain the following components */
/* (although structure component descriptors don't use them all.)  */
#define COMMONFIELDS  \
  decltype type;            /* Type of this declaration descriptor        */ \
  identdesc *ident;         /* pointer to associated ident. descriptor    */ \
  int line;                 /* Line number at which declaration occurred. */ \
  bool root_entry;          /* Is this entry a "root" entry?              */


/* Generic structure used to access common fields of decl. descriptors. */
struct unkdesc {
  COMMONFIELDS
};


/* Structure used for function declaration descriptors.    */
struct funcdesc {
  COMMONFIELDS
  union dcldesc * formallist;  /* head of list of this proc's formals.     */
  union dcldesc * rettype;     /* Return type of this function.            */
  int localsize;               /* Size of space required for locals.       */
  int paramsize;               /* Total size (in cells) of parameters      */
  union nodeunion * coderoot;  /* Root node for the code of this function. */
  vs * entry_vs;               /* vs with function entry point. */
  vs_pos * entry_pos;          /* Location of the actual entry point in the vs. */
  CODELBL * entrylbl;          /* Label placed on first line of proc.  hur??? */
};


/* Structure used to reference common fields of type descriptor.    */
struct unktypedesc {
  COMMONFIELDS
  int xtypesize, ytypesize;    /* Memory required for var. of this type. */
};


/* Structure used for array type declaration descriptors.     */
struct arraydesc {
  COMMONFIELDS
  int xtypesize, ytypesize;    /* Memory required for array of this type. */
  int size;                    /* Number of elements in array            */
  union dcldesc * vartype;  /* decl. descriptor for imaginary variable of
					   this array's element type      */
};


/* Structure used for struct type declaration descriptors.      */
struct structdesc {
  COMMONFIELDS
  int xtypesize, ytypesize;    /* Memory required for struct of this type. */
  union dcldesc *fields;       /* Header for list of this type's components */
};


/* Structure used for variable declaration descriptors.     */
struct vardesc {
  COMMONFIELDS
  union dcldesc * vartype;     /* decl. descriptor for the variable's type  */
  int offset;                  /* offset within current stack frame */
  int isGlobal;                /* Is this a global variable? */
};

/* Structure used for formal parameter declaration descriptors.    */
struct formaldesc {
  COMMONFIELDS
  union dcldesc * vartype;     /* decl. descriptor for the formal's type */
  union dcldesc * formallink;  /* link for list of proc's formals.       */
};

/* Structure used for structure field name declaration descriptors.   */
struct fielddesc {
  COMMONFIELDS
  union dcldesc * vartype;     /* decl. descriptor for component's type  */
  int offset;                  /* offset within current stack frame */
  union dcldesc * structtype;  /* decl. descriptor for type to which the 
								  component belongs                      */
  union dcldesc * fieldlink;   /* Next pointer for type's list of fields */
};

struct pointerdesc {
  COMMONFIELDS
  union dcldesc * vartype;     /* Type of object that it points to */
};

enum type_enum { INT_T, CHAR_T, LONG_T, VOID_T };

struct typedesc {
  COMMONFIELDS
  enum type_enum typetype;
};

/* This union describes the type of all declaration descriptors */
typedef union dcldesc {
  struct unkdesc unk;

  struct funcdesc func;

  struct vardesc unkvar;
  struct vardesc var;
  struct formaldesc formal;
  struct fielddesc field;

  struct unktypedesc unktype;
  struct arraydesc array;
  struct structdesc structure;
  struct pointerdesc pointer;
  struct typedesc type;
} dcldesc;

/*********************************************************************/
/*          End of specification of declaration descriptors          */
/*********************************************************************/

extern identdesc *int_node;
extern identdesc *char_node;
extern identdesc *long_node;
extern identdesc *void_node;

/* The following define makes it possible to use the name    */
/* INTEGER_TYPE to refer to the declaration descriptor for   */
/* the type integer.                                         */
#define INT_TYPE int_node->globaldecl
#define CHAR_TYPE char_node->globaldecl
#define LONG_TYPE long_node->globaldecl
#define VOID_TYPE void_node->globaldecl

/* Wrapper function for passing to DLL_Traverse. */
void printdcldesc_wrapper ( void * item );

/* printdcldesc --  Print out the information found in a declaration  */
/*                  descriptor.                                       */
void printdcldesc(dcldesc * desc, int indent);

/* printidentdesc --  Print out the information found in a ident  */
/*                    descriptor.                                 */
void printidentdesc(identdesc * desc, int indent);

/* lookup --- Given a string, this routine returns the identifier      */
/*            descriptor for that string (creating one if necessary.   */
identdesc *lookup(char * name);

identdesc *lookup_wo_insert(char * name);

/**********************************************************************/

void insert_special_hash_entries ();

dcldesc * insert_dcldesc ( decltype type, identdesc * id, int lineno );

dcldesc * process_formallist ( union nodeunion * root );
dcldesc * process_declarator ( union nodeunion * root );

void insert_special_symtab_entries ();
void mark_non_roots ( dcldesc * root, int level );
void mark_root_entries ();

/* Walk through the syntax tree and build up the symbol table. */
void build_symbol_table ();

#endif
