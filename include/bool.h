#ifndef __BOOL_H
#define __BOOL_H

typedef short bool;

#define true 1
#define false 0

#endif
