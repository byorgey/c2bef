/* dll.h

   Doubly-linked list ADT

   C-to-Befunge translation project

   (c) 2003 Steve Winslow and Brent Yorgey
*/

#ifndef __DLL_H
#define __DLL_H

#include "status_code.h"
#include "null.h"
#include "bool.h"

typedef void* Item_ptr;

typedef struct DLL_node
{
  Item_ptr item;
  struct DLL_node * next, * prev;
} DLL_node;

typedef DLL_node* DLL_node_ptr;

typedef struct DLL_struct
{
  long int num_elts;
  DLL_node_ptr head, tail;

  DLL_node_ptr current;     // for iterator methods
} DLL_struct;

typedef DLL_struct * DLL;

/* A few notes on the proposed implementation:

   If a procedure is called but the parameters do not meet the stated
     preconditions, in most cases the procedure exits without doing
     anything.  At the very least every effort will be made to "die
     gracefully", i.e. no crashes due to dangling pointers, NULL pointer
     dereferences, etc.
*/

/* DLL_Create  O(1)

   Pre:  None
   Post: Memory has been allocated and list initialized to empty
         Returns a pointer to the newly created list
	 ALWAYS CALL DLL_Destroy() to clean up a list allocated
  	   with DLL_Create()! Otherwise you'll have a memory leak. 
*/
DLL DLL_Create ();

/* DLL_Clear  O(n)

   Pre:  parameter list has been initialized with DLL_Create
   Post: parameter list points to an empty list
         free() is called for the item pointers of any nodes
	 returns OK on success, failure otherwise
*/
status_code DLL_Clear ( DLL list );

/* DLL_MakeEmpty   O(n)

   Like DLL_Clear, except that free() is NOT called for the item
   pointers.
*/
status_code DLL_MakeEmpty ( DLL list );

/* DLL_Destroy  O(n)

   Pre:  parameter list has been initialized
   Post: all memory associated with list has been freed
         Note: list is NOT set to NULL (it cannot be).
         Note: This is not the same as emptying the list.  The list
	   should not be used again before another call to DLL_Create.
	   To make a list empty, use DLL_Clear.
	 Returns OK on success, failure otherwise
*/
status_code DLL_Destroy ( DLL list );


/* DLL_IsEmpty  O(1)

   Pre:  list has been initialized
   Post: returns true iff list is empty, i.e. has zero elements.
*/
bool DLL_IsEmpty ( const DLL list );

/* DLL_Size  O(1)

   Pre:  list has been initialized
   Post: returns the number of elements contained in the list
*/
long int DLL_Size ( const DLL list );



/* DLL_Front  O(1)

   Pre:  list has been initialized
         list is non-empty
   Post: returns a pointer to the first element of the list
*/
Item_ptr DLL_Front ( const DLL list );

/* DLL_Back  O(1)

   Pre:  list has been initialized
         list is non-empty
   Post: returns a pointer to the last element of the list
*/
Item_ptr DLL_Back ( const DLL list );

/* DLL_ItemAt  O(n)

   Pre:  list has been initialized
         list contains at least (i+1) elements
   Post: returns a pointer to the item at index i, where indexing 
           starts at 0 for the first element.
*/
Item_ptr DLL_ItemAt ( const DLL list, long int i );

/* DLL_Find  O(n)

   Pre:  list has been initialized
         select is a function which selects items based on a 
           comparison with target
   Post: Returns a pointer to the first matching item, or NULL if no such
           item exists
*/
Item_ptr DLL_Select ( const DLL list, Item_ptr target,
		      bool (*select) (Item_ptr item1, Item_ptr item2) );

/* DLL_FindAll  O(n)

   Pre:  list has been initialized
         select is a function which selects items based on a 
           comparison with target
   Post: Returns a DLL containing pointers to all matching items, or
           NULL if no such items exist.
*/
DLL DLL_SelectAll ( const DLL list, Item_ptr target,
		    bool (*select) (Item_ptr item1, Item_ptr item2) );

/* DLL_InsertFront  O(1)
   
   Pre:  list has been initialized
   Post: the given item has been inserted at the front of the list.
         returns OK on success, otherwise overflow or failure
*/
status_code DLL_InsertFront ( DLL list, Item_ptr item );

/* DLL_InsertBack  O(1)

   Pre:  list has been initialized
   Post: the given item has been inserted at the end of the list.
         returns a pointer to the created node, or NULL if the insert failed.
*/
status_code DLL_InsertBack ( DLL list, Item_ptr item );

/* DLL_InsertAt  O(n)

   Pre:  list has been initialized
         list contains at least i elements
   Post: item has been inserted at index i in the list, where indexing
           starts with 0.  The item that used to have index i now
	   has index i+1, etc.  If i is equal to the number of 
	   elements in the list, the item is inserted at the end.
         returns OK on success, otherwise overflow or failure
*/
status_code DLL_InsertAt ( DLL list, Item_ptr item, long int index );

/* DLL_DeleteFront  O(1)

   Pre:  list has been initialized
         list is non-empty
   Post: The item at the front of the list has been taken out of the
           list, and the memory associated with it has been freed.
	 Returns OK on success or out_of_bounds if list is empty
*/
status_code DLL_DeleteFront ( DLL list );

/* DLL_DeleteBack  O(1)

   Pre:  list has been initialized
         list is non-empty
   Post: The item at the back of the list has been taken out of the
           list, and the memory associated with it has been freed.
	 Returns OK on success or out_of_bounds if list is empty
*/
status_code DLL_DeleteBack ( DLL list );

/* DLL_DeleteAt  O(n)

   Pre:  list has been initialized
         list has at least (i+1) elements
   Post: The item at index i of the  list has been taken out of the
           list, and the memory associated with it has been freed.
	   The item which originally had index (i+1) now has index
	   i, etc.
	 Returns OK on success or out_of_bounds if list is empty
*/
status_code DLL_DeleteAt ( DLL list, long int i );

/* DLL_DeleteRange  O(n)

   Pre:  list has been initialized
         j >= i
         list has at least (j+1) elements
   Post: The items with indices i through j inclusive have been 
           taken out of the list, and the memory associated with
	   them has been freed.
	 Returns OK on success or out_of_bounds if list is empty
*/
status_code DLL_DeleteRange ( DLL list, long int i, long int j );

/* DLL_SearchAndDestroy  O(n)

   Pre:  list has been initialized
   Post: The first item matching target has been deleted.
   	 Returns OK on success or not_found if no matching items were found
*/
status_code DLL_SearchAndDestroy ( DLL list, Item_ptr target, 
				   bool (*items_equal) (Item_ptr item1, Item_ptr item2) );

/* DLL_SearchAndDestryAll  O(n)

   Pre:  list has been initialized
   Post: All items matching target have been deleted.
   	 Returns OK on success or not_found if no matching items were found
*/
status_code DLL_SearchAndDestroyAll ( DLL list, Item_ptr target,
				      bool (*items_equal) (Item_ptr item1, Item_ptr item2) );



/* DLL_IterBegin  O(1)

   Pre:  list has been initialized
   Post: prepare to iterate through the list, i.e. set current 
           pointer to the front of the list
*/
void DLL_IterBegin ( DLL list );

/* DLL_IterAtEnd  O(1)

   Pre:  list has been initialized; DLL_IterBegin has been called
   Post: returns true iff no more elements remain to be iterated
           through, i.e. DLL_IterCurrent cannot be safely called.
*/
bool DLL_IterAtEnd ( const DLL list );

/* DLL_IterCurrent  O(1)

   Pre:  list has been initialized
         DLL_IterBegin has been called
	 DLL_IterAtEnd is false
   Post: Returns a pointer to the current item in the list
*/
Item_ptr DLL_IterCurrent ( const DLL list );

/* DLL_IterNext  O(1)

   Pre:  list has been initialized
         DLL_IterBegin has been called
	 DLL_IterAtEnd is false
   Post: Iterates to the next element in the list, i.e. sets
           current = current->next
*/
void DLL_IterNext ( DLL list );

/* Note: Iterator functions could be used something like this:

DLL myList;
myList = DLL_Create();

...

someType * item;
for ( DLL_IterBegin(myList); !DLL_IterAtEnd(myList); DLL_IterNext(myList) )
  item = (someType *)DLL_IterCurrent(myList);

*/


/* DLL_Traverse   O(n)

   Pre:  ProcessItem is a function which does something to each
           item of the list (e.g., print).
   Post: ProcessItem has been called on each item of the list, in order
           from front to back.
*/
void DLL_Traverse ( DLL list, void (*ProcessItem) (Item_ptr item) );


/* DLL_TraverseBackwards   O(n)
   Pre:  ProcessItem is a function which does something to each
           item of the list (e.g., print).
   Post: ProcessItem has been called on each item of the list, in order
           from back to front.
*/
void DLL_TraverseBackwards ( DLL list, void (*ProcessItem) (Item_ptr item) );

/* DLL_ConcatLists   O(1)

   Pre:  list1 and list2 have both been initialized
   Post: list2 is empty; the elements of list2 have been appended
           in order to list1
*/
void DLL_ConcatLists ( DLL list1, DLL list2 );

/* DLL_ReverseList   O(n)

   Pre:  list has been initialized
   Post: list has been reversed
*/
void DLL_ReverseList ( DLL list );

#endif



