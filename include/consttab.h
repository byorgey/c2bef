/* consttab.h

   Constant table.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#ifndef __CONSTTAB_H
#define __CONSTTAB_H

#include "dll.h"
#include "vs.h"
#include "beflib.h"

#include "symtab.h"

typedef enum { STATIC, FUNCLOC_X, FUNCLOC_Y } const_type;

typedef struct {

  vs * vscr;
  int x, y;
  long int value;

  /* For delayed computation of the constant... */
  const_type type;
  dcldesc * st_entry;

} ct_entry;

void init_const_table();

void destroy_const_table();

void print_ct_entry ( void * ctv );

/* Add an entry to the constant table. */
void add_constant ( vs * v, int x, int y, long val );

/* Add an entry to the constant table which is to be calculated later. */
void add_dynamic_constant ( vs * v, int x, int y, const_type t, dcldesc * st );

/* Add an entry to the constant table to deal with relative jumps. */
void add_reljump_constant ( vs * v, int x, int y, int jump_loc,
							const_type t, dcldesc * st );

/* Calculate all constants marked as needing calculation. */
void calculate_constants ();

/* Generate the constant flinger code.  Returns a pointer to the 
   LOWER LEFT CORNER vs. 
*/
vs * generate_flinger ();

#endif
