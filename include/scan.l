D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*

%{
#include <stdio.h>
#include "y.tab.h"

#include "symtab.h"
#include "syntreeops.h"

#define ECHO_SCAN 0

void count();

extern YYSTYPE yylval;

int lineno = 1;

%}

%%
"/*"			{ comment(); }

"auto"			{ count(); return(AUTO); }
"break"			{ count(); return(BREAK); }
"case"			{ count(); return(CASE); }
"char"			{ count(); return(CHAR); }
"const"			{ count(); return(CONST); }
"continue"		{ count(); return(CONTINUE); }
"default"		{ count(); return(DEFAULT); }
"do"			{ count(); return(DO); }
"double"		{ count(); return(DOUBLE); }
"else"			{ count(); return(ELSE); }
"enum"			{ count(); return(ENUM); }
"extern"		{ count(); return(EXTERN); }
"float"			{ count(); return(FLOAT); }
"for"			{ count(); return(FOR); }
"goto"			{ count(); return(GOTO); }
"if"			{ count(); return(IF); }
"int"			{ count(); return(INT); }
"long"			{ count(); return(LONG); }
"register"		{ count(); return(REGISTER); }
"return"		{ count(); return(RETURN); }
"short"			{ count(); return(SHORT); }
"signed"		{ count(); return(SIGNED); }
"sizeof"		{ count(); return(SIZEOF); }
"static"		{ count(); return(STATIC); }
"struct"		{ count(); return(STRUCT); }
"switch"		{ count(); return(SWITCH); }
"typedef"		{ count(); return(TYPEDEF); }
"union"			{ count(); return(UNION); }
"unsigned"		{ count(); return(UNSIGNED); }
"void"			{ count(); return(VOID); }
"volatile"		{ count(); return(VOLATILE); }
"while"			{ count(); return(WHILE); }

{L}({L}|{D})*		{ count(); return(process_ident()); }

0[xX]{H}+{IS}?		{ count(); return(process_hex_constant()); }
0{D}+{IS}?		{ count(); return(process_octal_constant()); }
{D}+{IS}?		{ count(); return(process_int_constant()); }
'(\\.|[^\\'])+'		{ count(); return(process_char_constant()); }

{D}+{E}{FS}?		{ count(); return(process_float_constant()); }
{D}*"."{D}+({E})?{FS}?	{ count(); return(process_float_constant()); }
{D}+"."{D}*({E})?{FS}?	{ count(); return(process_float_constant()); }

\"(\\.|[^\\"])*\"	{ count(); return(process_string_literal()); }

">>="			{ count(); return(RIGHT_ASSIGN); }
"<<="			{ count(); return(LEFT_ASSIGN); }
"+="			{ count(); return(ADD_ASSIGN); }
"-="			{ count(); return(SUB_ASSIGN); }
"*="			{ count(); return(MUL_ASSIGN); }
"/="			{ count(); return(DIV_ASSIGN); }
"%="			{ count(); return(MOD_ASSIGN); }
"&="			{ count(); return(AND_ASSIGN); }
"^="			{ count(); return(XOR_ASSIGN); }
"|="			{ count(); return(OR_ASSIGN); }
">>"			{ count(); return(RIGHT_OP); }
"<<"			{ count(); return(LEFT_OP); }
"++"			{ count(); return(INC_OP); }
"--"			{ count(); return(DEC_OP); }
"->"			{ count(); return(PTR_OP); }
"&&"			{ count(); return(AND_OP); }
"||"			{ count(); return(OR_OP); }
"<="			{ count(); return(LE_OP); }
">="			{ count(); return(GE_OP); }
"=="			{ count(); return(EQ_OP); }
"!="			{ count(); return(NE_OP); }
";"			{ count(); return(';'); }
"{"			{ count(); return('{'); }
"}"			{ count(); return('}'); }
","			{ count(); return(','); }
":"			{ count(); return(':'); }
"="			{ count(); return('='); }
"("			{ count(); return('('); }
")"			{ count(); return(')'); }
"["			{ count(); return('['); }
"]"			{ count(); return(']'); }
"."			{ count(); return('.'); }
"&"			{ count(); return('&'); }
"!"			{ count(); return('!'); }
"~"			{ count(); return('~'); }
"-"			{ count(); return('-'); }
"+"			{ count(); return('+'); }
"*"			{ count(); return('*'); }
"/"			{ count(); return('/'); }
"%"			{ count(); return('%'); }
"<"			{ count(); return('<'); }
">"			{ count(); return('>'); }
"^"			{ count(); return('^'); }
"|"			{ count(); return('|'); }
"?"			{ count(); return('?'); }

[ \t\v\f]		{ count(); }
"\n"                    { count(); lineno++; }
.			{ /* ignore bad characters */ }

%%

yywrap()
{
	return(1);
}

comment()
{
	char c, c1;

loop:
	while ((c = input()) != '*' && c != 0)
		putchar(c);

	if ((c1 = input()) != '/' && c != 0)
	{
		unput(c1);
		goto loop;
	}

	if (c != 0)
		putchar(c1);
}

int column = 0;

void count()
{
	int i;

	for (i = 0; yytext[i] != '\0'; i++)
		if (yytext[i] == '\n')
			column = 0;
		else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;

#if ECHO_SCAN
	ECHO;
#endif
		
        /* Return the line number to the parser */
        yylval.lexeme.line = lineno;
}

int process_ident()
{

/* Insert identifier into the ID hash, or get a pointer to it if
   already there */

  identdesc * id = lookup ( yytext );

  yylval.lexeme.identptr = id;

  return IDENTIFIER;
}

int process_hex_constant()
{
  return NOT_YET;
}

int process_octal_constant()
{
  return NOT_YET;
}

int process_int_constant()
{
  yylval.lexeme.constval = atoi(yytext);
  yylval.lexeme.consttype = 2;

  return CONSTANT;
}

int process_char_constant()
{
  char c = yytext[1];

  if ( c == '\\' ) {

    switch ( yytext[2] ) {
 
    case 't': c = '\t'; break;
    case 'v': c = '\v'; break;
    case 'b': c = '\b'; break;
    case 'r': c = '\r'; break;
    case 'f': c = '\f'; break;
    case 'a': c = '\a'; break;
    case '\\': c = '\\'; break;
    case '?': c = '?'; break;
    case '\'': c = '\''; break;
    case '"': c = '"'; break;
    case 'n': c = '\n'; break;
      
    }
  }
  
  yylval.lexeme.constval = c;
  yylval.lexeme.consttype = 1;

  return CONSTANT;
}

int process_float_constant()
{
  return NOT_YET;
}

int process_string_literal()
{
  char * iterator;
  char * iterator2;
  char c;
  yylval.lexeme.strptr = (char *) malloc ( strlen(yytext) * sizeof(char) );
  strcpy ( yylval.lexeme.strptr, yytext + 1);

  /* Kill the ending quote. Kill it dead! */
  iterator = yylval.lexeme.strptr;
  while (*iterator != '\0') iterator++;
  *--iterator = '\0';

  iterator = yylval.lexeme.strptr;
  iterator2 = yylval.lexeme.strptr;

  /* use iterator as the read pointer, iterator2 as the writer */

  while (*iterator != '\0') {
	if (*iterator == '\\') {
	  /* escape sequence */
	  iterator++;
	  c = *iterator;
	  switch (c) {
	  case 't': c = '\t'; break;
	  case 'v': c = '\v'; break;
	  case 'b': c = '\b'; break;
	  case 'r': c = '\r'; break;
	  case 'f': c = '\f'; break;
	  case 'a': c = '\a'; break;
	  case '\\': c = '\\'; break;
	  case '?': c = '?'; break;
	  case '\'': c = '\''; break;
	  case '"': c = '"'; break;
	  case 'n': c = '\n'; break;
	  default: break;
	  }
	  *iterator2 = c;
	} else {
	  *iterator2 = *iterator;
	}
	iterator++;
	iterator2++;
  }

  *iterator2 = '\0';
	  
  return STRING_LITERAL;
}
