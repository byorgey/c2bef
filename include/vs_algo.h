/* vs_algo.h

   Various yummy algorithms for doing things with
   vs-gloms.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#ifndef __VS_ALGO_H
#define __VS_ALGO_H

#include "vs.h"
#include "dll.h"

/* Unvisit all vs's in the given glom, assuming that all visited vs's
   are contiguous. 
*/
void unvisit ( vs * screen );

/* Given any vs in a glom, calculate the distance rightwards (in
   number of intervening vs's, including itself) to the smallest
   rectangle enclosing the entire glom at each vs.  Store the values
   in the corresponding fields inside each vs.
*/
void calc_max_right ( vs * screen );    

/* Similar, for downwards. */
void calc_max_down ( vs * screen );

/* Calculate global width and height of a glom.  These assume that
   calc_max_right and calc_max_down have already been called. */
int calc_width ( vs * screen );
int calc_height ( vs * screen );

void fill_blanks ( vs * screen );

/* Deallocate all the memory used by a glom. */
void destroy_glom ( vs * screen );

/* Attach all the vs's in the list together into one big glom. */
vs * glomify ( DLL vs_list );

#endif
