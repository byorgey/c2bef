/* nodetype.h
   Node type definitions for syntax tree nodes

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#ifndef __NODETYPE_H
#define __NODETYPE_H

/* Any node ending with "list" has the following format:

   child[0] points to a specific item in the list
   child[1] points to another list node of the same type, or NULL to
     denote the end of the list.
*/

typedef enum nodetype {

  Nerror = 0,

  /* Special node types */

  Nident,     /* identifier node: pointer into the identifier hash */
  Nconst,     /* constant node */

  Nrefvar,    /* child[0] is either an Nident node, OR 
				 an expression yielding an address ??? */
  Nstring,    /* string literals */
  
  /* All other (internal) node types */

  /* The root node is always an Ndefnlist. */

  Ndefnlist,  /* List of external definitions.
		 Each item is either an Ndecl or an Nfuncdefn.
	      */

  /* Compound declarator types.  (array, pointer, funcsig, struct)
       child[0] is another compound declarator type  OR
                   an Nident node  OR
                   NULL if the type is anonymous.
       child[1] contains information specific to the type (see below).

     Note that a "declarator node" refers to either a compound
     declarator node or an Nident node.
   */
  Narray,     /* child[1] is an expression giving the size of the array, or
                 NULL if unsized */
  Npointer,   /* child[1] is NULL. */
  Nfuncsig,   /* child[1] is an Nformallist. */
  Nstruct,    /* child[1] is an Nfieldlist.  */

  Nformallist, /* A list of formal parameters. 
                  Each item is an Nparam. */
  Nparam,     /* child[0] is an Nident giving the base type, OR
                             NULL for untyped parameters (whatever that means).
                 child[1] is a declarator node.
	      */

  Nfieldlist, /* child[0] is an Ndecl. */

  Ndecl,      /* A single declaration statement.
                 child[0] is a declarator node giving the base type.
                 child[1] is an Ninitdecllist. */
  Ninitdecllist,  /* each item can be EITHER an Ninitdecl node 
                                  OR a declarator node. */

  Ninitdecl,  /* Declaration with initialization.
                 child[0] is a declarator node
                 child[1] is an Ninitlist. */
  Ninitlist,  /* A list of initializers.
                 each item is an expression. */
  
  /* Some more lists */
  Ndecllist,  /* Each item is an Ndecl. */
  
  Nfuncdefn,  /* A function definition.
                 child[0] is an Nident giving the base type.
                 child[1] is an Nfuncsig.
                 child[2] is an Ncompound.
	      */
  
  /* Statements. */
  
  Nstmtlist,   /* List of statements of the following types: 
                    Left child is the next statement (can also be
		      another Nstmtlist) 
                    Right child is another stmtlist */

  Ncompound,   /* Compound statement
	          child[0] is an Ndecllist
                  child[1] is an Nstmtlist */

  Ncall,       /* Function call.
                  child[0] is an Nident with the function name.
                  child[1] is an Nactuallist */

  Nreturn,     /* child[0] is an expression for the return value */

  Nif,         /* child[0] is the expression to be tested
				  child[1] is evaluated if child[0] evaluates to true
			      child[2] is evaluated if else (may be NULL) */

  Nwhile,      /* child[0] is the expression to be tested
				  child[1] is executed until child[0] is false
			        (may be an Ncompound or an Nstmtlist) */ 

  Ncontinue,   /* no children */
  Nbreak,      /* no children */

  /* End of statement nodes */

  Nactuallist, /* actual parameter list.
                  Each item is an expression. */

  /* Expressions */
  
  Nselect,     /* used to select values from a struct
				  child[0] is a refvar pointing to the name of the struct
			      child[1] is an Nident giving the name of the member */

  Nsubs,       /* used to reference members of an array
				  child[0] is an expression evaluating to the name of the array
			      child[1] is an expression evaluating the the desired subscript */

  /* gets operators */
  Ngets,       /* child[0] should be a refvarnode */
  Ntimesgets,
  Ndivgets,
  Nplusgets,
  Nminusgets,
  Nmodgets,
  Nshiftrgets,
  Nshiftlgets,
  Nandgets,
  Norgets,
  Nxorgets,
  
  /* unary operators */
  Nnot,     /* !n */
  Nneg,     /* -n */
  Npos,     /* +n */
  Nbitneg,  /* ~n */
  Naddrof,  /* &n */
  Nderef,   /* *n */
  Nincpre,  /* ++n */
  Ndecpre,  /* --n */
  Nincpost, /* n++ */
  Ndecpost, /* n-- */
  Nsizeof,  /* sizeof(n) */
  
  
  /* binary operators */
  Nor,
  Nand,
  Nbitor,    /* bitwise operators */
  Nbitand,
  Nbitxor,
  Nshiftleft,  /* shift operators */
  Nshiftright, 
  Nlt,
  Ngt,
  Neq,
  Nle,
  Nge,
  Nne,
  Nplus,
  Nminus,
  Ntimes,
  Ndiv,
  Nmod,

  /* ternary operators */
  Nstupidop   /* ? : */

} nodetype;

#endif
