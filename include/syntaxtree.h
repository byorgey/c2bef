/* syntaxtree.h

   Type declarations for syntax trees.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#ifndef __SYNTAXTREE_H
#define __SYNTAXTREE_H

#define MAXKIDS 3

#include "nodetype.h"
#include "symtab.h"

/* Pronounced "unc-node". */
struct unknode {
  nodetype type;
  int line;
};

struct internalnode {
  nodetype type;
  int line;
  union nodeunion *child[MAXKIDS]; /* pointers to the node's sub-trees */
};


/* ident points into the hash generated during syntactical analysis.
   decl points into the symbol table generated during semantic analysis.
*/
struct identnode {
  nodetype type;
  int line;
  identdesc *ident;   /* Pointer to associated identifier descriptor */
  dcldesc *decl;     /* Pointer to associated declaration descriptor */
};


struct constnode {
  nodetype type;
  int line;
  int value;               /* Integer value of the constant  */
  int ischar;              /* True if this was a character constant */
};


struct refvarnode {
  nodetype type;
  int line;
  union nodeunion * baseaddr;      /* Subtree describing base address calculation */
  dcldesc *vardesc;    /* Declaration descriptor for referenced variable    */
};

struct stringconstnode {
  nodetype type;
  int line;
  char * strvalue;
};

typedef union nodeunion {
  struct unknode unk;
  struct internalnode internal;
  struct identnode ident;
  struct constnode constant;
  struct refvarnode var;
  struct stringconstnode string;
} node;

/* This is THE syntax tree */
extern node * syntaxtree;

#endif
  
