%token <lexeme> IDENTIFIER CONSTANT STRING_LITERAL 
%token <treenode> SIZEOF
%token <treenode> PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token <treenode> AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token <treenode> SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token <treenode> XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token <treenode> TYPEDEF EXTERN STATIC AUTO REGISTER
%token <treenode> CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token <treenode> STRUCT UNION ENUM ELIPSIS RANGE

%token <treenode> CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token <lexeme> NOT_YET


%type <flag> unary_operator assignment_operator

%type <treenode> primary_expr postfix_expr argument_expr_list 
%type <list> argument_expr_list_l
%type <treenode> unary_expr cast_expr multiplicative_expr additive_expr shift_expr relational_expr
%type <treenode> equality_expr and_expr exclusive_or_expr inclusive_or_expr logical_and_expr
%type <treenode> logical_or_expr conditional_expr assignment_expr expr constant_expr

%type <treenode> declaration declaration_specifiers
%type <treenode> init_declarator_list
%type <list> init_declarator_list_l
%type <treenode> init_declarator

%type <treenode> type_specifier struct_or_union_specifier
%type <flag> struct_or_union

%type <treenode> struct_declaration_list
%type <list> struct_declaration_list_l
%type <treenode> struct_declaration
%type <treenode> struct_declarator_list
%type <list> struct_declarator_list_l
%type <treenode> struct_declarator

%type <treenode> declarator declarator2 pointer

%type <treenode> type_specifier_list

%type <treenode> parameter_identifier_list identifier_list
%type <list> identifier_list_l
%type <treenode> parameter_type_list
%type <treenode> parameter_list
%type <list> parameter_list_l
%type <treenode> parameter_declaration

%type <treenode> type_name

%type <treenode> abstract_declarator abstract_declarator2

%type <treenode> initializer
%type <list> initializer_list

%type <treenode> statement labeled_statement compound_statement

%type <treenode> declaration_list
%type <list> declaration_list_l

%type <treenode> statement_list
%type <list> statement_list_l

%type <treenode> expression_statement selection_statement 
%type <treenode> iteration_statement jump_statement

%type <treenode> file external_definition
%type <list> file_l

%type <treenode> function_body function_definition

%type <treenode> identifier

%{
#include "syntreeops.h"

/* A useful empty-list "constant" */
extern struct listhead empty_list;

/* #define __PARSE_DEBUG*/

#ifdef __PARSE_DEBUG
#define printd(x...) printf(x)
#else
#define printd(x...)
#endif

%}

%start file
%%

primary_expr
	: identifier
          { $$ = makerefvar($1, NULL, lineno); }
	| CONSTANT
          { $$ = makeconst (yylval.lexeme.constval,yylval.lexeme.consttype == 1, lineno); }
	| STRING_LITERAL
          { $$ = makestring (yylval.lexeme.strptr, lineno); }
	| '(' expr ')'
          { $$ = $2; }
	;

postfix_expr
	: primary_expr
          { $$ = $1; }
	| postfix_expr '[' expr ']'
          { $$ = makedouble(Nsubs, $1, $3, lineno); }
	| postfix_expr '(' ')'
          { node * tempNode = makerefvar($1, NULL, lineno);
            $$ = makedouble(Ncall, tempNode, NULL, lineno);
          }
	| postfix_expr '(' argument_expr_list ')'
          { node * tempNode = makerefvar($1, NULL, lineno);
            $$ = makedouble(Ncall, tempNode, $3, lineno);
          }
	| postfix_expr '.' identifier
          { node * tempNode = makerefvar($1, NULL, lineno);
            $$ = makedouble(Nselect, tempNode, $3, lineno);
          }
	| postfix_expr PTR_OP identifier
          { node * tempNode = makesingle(Nderef, $1, lineno);
            node * tempNode2 = makerefvar(tempNode, NULL, lineno);
            $$ = makedouble(Nselect, tempNode2, $3, lineno);
          }
	| postfix_expr INC_OP
          { $$ = makesingle (Nincpost, $1, lineno); }
	| postfix_expr DEC_OP
          { $$ = makesingle (Ndecpost, $1, lineno); }
        ;

argument_expr_list
	: argument_expr_list_l
          { $$ = $1.head; }
	;

argument_expr_list_l
	: assignment_expr
          { $$ = makelist ( empty_list, makedouble ( Nactuallist, $1, NULL, lineno ), lineno ); }
	| argument_expr_list_l ',' assignment_expr
          { $$ = makelist ( $1, makedouble ( Nactuallist, $3, NULL, lineno ), lineno ); }
	;

unary_expr
	: postfix_expr
          { $$ = $1; }
	| INC_OP unary_expr
          { $$ = makesingle (Nincpre, $2, lineno); }
	| DEC_OP unary_expr
          { $$ = makesingle (Ndecpre, $2, lineno); }
	| unary_operator cast_expr
          { $$ = makesingle ($1, $2, lineno); }
	| SIZEOF unary_expr
          { $$ = makesingle (Nsizeof, $2, lineno); }
	| SIZEOF '(' type_name ')'
          { $$ = makesingle (Nsizeof, $3, lineno); }
	;

unary_operator
	: '&'
	  { $$ = Naddrof; }
	| '*'
          { $$ = Nderef; }
	| '+'
          { $$ = Npos; }
	| '-'
          { $$ = Nneg; }
	| '~'
          { $$ = Nbitneg; }
	| '!'
          { $$ = Nnot; }
	;

cast_expr
	: unary_expr
          { $$ = $1; }
	| '(' type_name ')' cast_expr
          { $$ = errornode (lineno); }
	;

multiplicative_expr
	: cast_expr
          { $$ = $1; }
	| multiplicative_expr '*' cast_expr
          { $$ = makedouble (Ntimes, $1, $3, lineno); }
	| multiplicative_expr '/' cast_expr
          { $$ = makedouble (Ndiv, $1, $3, lineno); }
	| multiplicative_expr '%' cast_expr
          { $$ = makedouble (Nmod, $1, $3, lineno); }
	;

additive_expr
	: multiplicative_expr 
          { $$ = $1; }
	| additive_expr '+' multiplicative_expr
          { $$ = makedouble (Nplus, $1, $3, lineno); }
	| additive_expr '-' multiplicative_expr
          { $$ = makedouble (Nminus, $1, $3, lineno); }
	;

shift_expr
	: additive_expr
          { $$ = $1; }
	| shift_expr LEFT_OP additive_expr
          { $$ = makedouble (Nshiftleft, $1, $3, lineno); }
	| shift_expr RIGHT_OP additive_expr
          { $$ = makedouble (Nshiftright, $1, $3, lineno); }
	;

relational_expr
	: shift_expr
          { $$ = $1; }
	| relational_expr '<' shift_expr
          { $$ = makedouble (Nlt, $1, $3, lineno); }
	| relational_expr '>' shift_expr
          { $$ = makedouble (Ngt, $1, $3, lineno); }
	| relational_expr LE_OP shift_expr
          { $$ = makedouble (Nle, $1, $3, lineno); }
	| relational_expr GE_OP shift_expr
          { $$ = makedouble (Nge, $1, $3, lineno); }
	;

equality_expr
	: relational_expr
          { $$ = $1; }
	| equality_expr EQ_OP relational_expr
          { $$ = makedouble (Neq, $1, $3, lineno); }
	| equality_expr NE_OP relational_expr
          { $$ = makedouble (Nne, $1, $3, lineno); }
	;

and_expr
	: equality_expr
          { $$ = $1; }
	| and_expr '&' equality_expr
          { $$ = makedouble (Nand, $1, $3, lineno); }
	;

exclusive_or_expr
	: and_expr
          { $$ = $1; }
	| exclusive_or_expr '^' and_expr
          { $$ = makedouble (Nbitxor, $1, $3, lineno); }
	;

inclusive_or_expr
	: exclusive_or_expr
          { $$ = $1; }
	| inclusive_or_expr '|' exclusive_or_expr
          { $$ = makedouble (Nbitor, $1, $3, lineno); }
	;

logical_and_expr
	: inclusive_or_expr
          { $$ = $1; }
	| logical_and_expr AND_OP inclusive_or_expr
          { $$ = makedouble (Nand, $1, $3, lineno); }
	;

logical_or_expr
	: logical_and_expr
          { $$ = $1; }
	| logical_or_expr OR_OP logical_and_expr
          { $$ = makedouble (Nor, $1, $3, lineno); }
	;

conditional_expr
	: logical_or_expr
          { $$ = $1; }
	| logical_or_expr '?' logical_or_expr ':' conditional_expr
	  { $$ = maketriple ( Nstupidop, $1, $3, $5, lineno ); }
	;

assignment_expr
	: conditional_expr
          { $$ = $1; }
	| unary_expr assignment_operator assignment_expr
	  { $$ = makedouble($2,$1,$3,lineno); }
	;

assignment_operator
	: '='
          { $$ = Ngets; }
	| MUL_ASSIGN
          { $$ = Ntimesgets; }
	| DIV_ASSIGN
          { $$ = Ndivgets; }
	| MOD_ASSIGN
          { $$ = Nmodgets; }
	| ADD_ASSIGN
          { $$ = Nplusgets; }
	| SUB_ASSIGN
          { $$ = Nminusgets; }
	| LEFT_ASSIGN
          { $$ = Nshiftlgets; }
	| RIGHT_ASSIGN
          { $$ = Nshiftrgets; }
	| AND_ASSIGN
          { $$ = Nandgets; }
	| XOR_ASSIGN
          { $$ = Nxorgets; }
	| OR_ASSIGN
          { $$ = Norgets; }
	;

expr
	: assignment_expr
          { $$ = $1; }
	| expr ',' assignment_expr
          { $$ = errornode (lineno); }
	;

constant_expr
	: conditional_expr
          { $$ = $1; }
	;

declaration
	: declaration_specifiers ';'
          { $$ = makedouble ( Ndecl, $1, makedouble ( Ninitdecllist, NULL, NULL, lineno ), lineno ); }
	| declaration_specifiers init_declarator_list ';'
          { $$ = makedouble ( Ndecl, $1, $2, lineno ); }
	;

declaration_specifiers
	: storage_class_specifier
          { $$ = errornode ( lineno ); }
	| storage_class_specifier declaration_specifiers
          { $$ = errornode ( lineno ); }
	| type_specifier
          { $$ = $1; }
	| type_specifier declaration_specifiers
          { $$ = errornode ( lineno ); }
	;

init_declarator_list
        : init_declarator_list_l
          { $$ = $1.head; }
        ;

init_declarator_list_l
	: init_declarator
          { $$ = makelist ( empty_list, makedouble ( Ninitdecllist, $1, NULL, lineno ), lineno ); }
	| init_declarator_list_l ',' init_declarator
          { $$ = makelist ( $1, makedouble ( Ninitdecllist, $3, NULL, lineno ), lineno ); }
	;

init_declarator
	: declarator
          { $$ = $1; }
	| declarator '=' initializer
          { $$ = makedouble ( Ninitdecl, $1, $3, lineno ); }
	;

storage_class_specifier
	: TYPEDEF
	| EXTERN
	| STATIC
	| AUTO
	| REGISTER
	;

type_specifier
	: CHAR
	  { $$ = makeident ( lookup("char"), lineno ); } 
	| SHORT
	  { $$ = errornode ( lineno ); }
	| INT
          { $$ = makeident ( lookup("int"), lineno ); } 
	| LONG
 	  { $$ = makeident ( lookup("long"), lineno ); } 
	| SIGNED
	  { $$ = errornode ( lineno ); }
	| UNSIGNED
	  { $$ = errornode ( lineno ); }
	| FLOAT
	  { $$ = errornode ( lineno ); }
	| DOUBLE
	  { $$ = errornode ( lineno ); }
	| CONST
	  { $$ = errornode ( lineno ); }
	| VOLATILE
	  { $$ = errornode ( lineno ); }
	| VOID
	  { $$ = makeident ( lookup("void"), lineno ); } 
	| struct_or_union_specifier
          { $$ = $1; }
	| enum_specifier
	  { $$ = errornode ( lineno ); }
	| TYPE_NAME
	  { $$ = errornode ( lineno ); }
	;

struct_or_union_specifier
	: struct_or_union identifier '{' struct_declaration_list '}'
          { $$ = makedouble ( $1, $2, $4, lineno ); }
	| struct_or_union '{' struct_declaration_list '}'
          { $$ = makedouble ( $1, NULL, $3, lineno ); }
	| struct_or_union identifier
          { $$ = makedouble ( $1, $2, NULL, lineno ); }
	;

struct_or_union
	: STRUCT
          { $$ = Nstruct; }
	| UNION
          { $$ = Nerror; }
	;

struct_declaration_list
        : struct_declaration_list_l
          { $$ = $1.head; }
        ;

struct_declaration_list_l
	: struct_declaration
          { $$ = makelist ( empty_list, makedouble ( Nfieldlist, $1, NULL, lineno ), lineno ); }
	| struct_declaration_list_l struct_declaration
          { $$ = makelist ( $1, makedouble ( Nfieldlist, $2, NULL, lineno ), lineno ); }
	;

struct_declaration
	: type_specifier_list struct_declarator_list ';'
          { $$ = makedouble ( Ndecl, $1, $2, lineno ); }
	;

struct_declarator_list
        : struct_declarator_list_l
          { $$ = $1.head; }

struct_declarator_list_l
	: struct_declarator
          { $$ = makelist ( empty_list, makedouble ( Ninitdecllist, $1, NULL, lineno ), lineno ); }
	| struct_declarator_list_l ',' struct_declarator
          { $$ = makelist ( $1, makedouble ( Ninitdecllist, $3, NULL, lineno ), lineno ); }
	;

struct_declarator
	: declarator
          { $$ = $1; }
	| ':' constant_expr
          { $$ = errornode ( lineno ); /* ??? */ }
	| declarator ':' constant_expr
          { $$ = errornode ( lineno ); /* ??? */ }
	;

enum_specifier
	: ENUM '{' enumerator_list '}'
	| ENUM identifier '{' enumerator_list '}'
	| ENUM identifier
	;

enumerator_list
	: enumerator
	| enumerator_list ',' enumerator
	;

enumerator
	: identifier
	| identifier '=' constant_expr
	;

declarator
	: declarator2
	  { printd("declarator2\n");
            $$ = $1; }
	| pointer declarator2
          {
            node * ptr_chain = $1;
            node * scan = ptr_chain;
            while ( scan->internal.child[0] != NULL )
              scan = scan->internal.child[0];

            if ( scan->unk.type != Nerror && 
                 scan->unk.type != Npointer  ) {
              printf ( "Ack!! Abandon ship!!\n" );
              exit(1);
            }

            scan->internal.child[0] = $2;
          }  
	;

declarator2
	: identifier
          { printd("identifier\n");
            $$ = $1; }
	| '(' declarator ')'
          { printd("( declarator )\n");
            $$ = $2; }
	| declarator2 '[' ']'
          { printd("declarator2 [ ]\n");
            $$ = makedouble ( Narray, $1, NULL, lineno ); }
	| declarator2 '[' constant_expr ']'
          { printd("declarator2 [ constant_expr ]\n");
            $$ = makedouble ( Narray, $1, $3, lineno ); }
	| declarator2 '(' ')'
          { printd("declarator2 ( )\n");
            $$ = makedouble ( Nfuncsig, $1, NULL, lineno ); }
	| declarator2 '(' parameter_type_list ')'
          { printd("declarator2 ( parameter_type_list )\n");
            $$ = makedouble ( Nfuncsig, $1, $3, lineno ); }
	| declarator2 '(' parameter_identifier_list ')'
          { printd("declarator2 ( parameter_identifier_list )\n");
            $$ = makedouble ( Nfuncsig, $1, $3, lineno ); }
	;

pointer
	: '*'
	  { printd ( "*\n" ); 
            $$ = makesingle ( Npointer, NULL, lineno ); }
	| '*' type_specifier_list
	  { $$ = errornode ( lineno ); }
	| '*' pointer
	  { printd ( "* pointer\n" );
            $$ = makesingle ( Npointer, $2, lineno ); }
	| '*' type_specifier_list pointer
	  { deletetree ( $3 );
            $$ = errornode ( lineno ); }
	;

type_specifier_list
	: type_specifier
          { $$ = $1; }
	| type_specifier_list type_specifier
          { $$ = errornode ( lineno ); }
	;

parameter_identifier_list
	: identifier_list
          { $$ = $1; }
	| identifier_list ',' ELIPSIS
          { $$ = errornode ( lineno ); }
	;

identifier_list
        : identifier_list_l
          { $$ = $1.head; }

identifier_list_l
	: identifier
          { node * paramnode = makedouble ( Nparam, NULL, $1, lineno );
            $$ = makelist ( empty_list, makedouble ( Nformallist, paramnode, NULL, lineno ), lineno ); }
	| identifier_list_l ',' identifier
          { node * paramnode = makedouble ( Nparam, NULL, $3, lineno );
            $$ = makelist ( $1, makedouble ( Nformallist, paramnode, NULL, lineno ), lineno ); }
	;

parameter_type_list
	: parameter_list
          { $$ = $1; }
	| parameter_list ',' ELIPSIS
          { $$ = errornode ( lineno ); }
	;

parameter_list
        : parameter_list_l
          { $$ = $1.head; }
        ;

parameter_list_l
	: parameter_declaration
          { $$ = makelist ( empty_list, makedouble ( Nformallist, $1, NULL, lineno ), lineno ); }
	| parameter_list_l ',' parameter_declaration
          { $$ = makelist ( $1, makedouble ( Nformallist, $3, NULL, lineno ), lineno ); }
	;

parameter_declaration
	: type_specifier_list declarator
          { $$ = makedouble ( Nparam, $1, $2, lineno ); }
	| type_name
          { $$ = $1; }
	;

type_name
	: type_specifier_list
          { $$ = makedouble ( Nparam, $1, NULL, lineno ); }
	| type_specifier_list abstract_declarator
          { $$ = makedouble ( Nparam, $1, $2, lineno ); }
	;

abstract_declarator
	: pointer
          { $$ = makesingle ( Npointer, NULL, lineno ); }
	| abstract_declarator2
          { $$ = $1; }
	| pointer abstract_declarator2
          { $$ = errornode ( lineno ); /* ??? */ }
	;

abstract_declarator2
	: '(' abstract_declarator ')'
          { printd("( abstract_declarator )\n");
            $$ = $2; }
	| '[' ']'
          { printd("[ ]\n");
            $$ = makedouble ( Narray, NULL, NULL, lineno ); }
	| '[' constant_expr ']'
          { printd("[ constant_expr ]\n");
            $$ = makedouble ( Narray, NULL, $2, lineno ); }
	| abstract_declarator2 '[' ']'
          { printd("abstract_declarator2 [ ]\n");
            $$ = makedouble ( Narray, $1, NULL, lineno ); }
	| abstract_declarator2 '[' constant_expr ']'
          { printd("abstract_declarator2 [ constant_expr ]\n");
            $$ = makedouble ( Narray, $1, $3, lineno ); }
	| '(' ')'
          { printd("( )\n");
            $$ = makedouble ( Nfuncsig, NULL, NULL, lineno ); } 
	| '(' parameter_type_list ')'
          { printd("( parameter_type_list )\n");
            $$ = makedouble ( Nfuncsig, NULL, $2, lineno ); }
	| abstract_declarator2 '(' ')'
          { printd("abstract_declarator2 ( )\n");
            $$ = makedouble ( Nfuncsig, $1, NULL, lineno ); }
	| abstract_declarator2 '(' parameter_type_list ')'
          { printd("abstract_declarator2 ( parameter_type_list )\n");
            $$ = makedouble ( Nfuncsig, $1, $3, lineno ); }
	;

initializer
	: assignment_expr
          { $$ = makedouble ( Ninitlist, $1, NULL, lineno ); }
	| '{' initializer_list '}'
          { $$ = $2.head; }
	| '{' initializer_list ',' '}'
          { $$ = $2.head; }
	;

initializer_list
	: initializer
          { $$ = makelist ( empty_list, makedouble ( Ninitlist, $1, NULL, lineno ), lineno ); }
	| initializer_list ',' initializer
          { $$ = makelist ( $1, makedouble ( Ninitlist, $3, NULL, lineno ), lineno ); }
	;

statement
	: labeled_statement
          { $$ = $1; }
	| compound_statement
          { $$ = $1; }
	| expression_statement
          { $$ = $1; }
	| selection_statement
          { $$ = $1; }
	| iteration_statement
          { $$ = $1; }
	| jump_statement
          { $$ = $1; }
	;

labeled_statement
	: identifier ':' statement
          { $$ = errornode (lineno); }
	| CASE constant_expr ':' statement
          { $$ = errornode (lineno); }
	| DEFAULT ':' statement
          { $$ = errornode (lineno); }
	;

compound_statement
	: '{' '}'
          { $$ = makedouble ( Ncompound, NULL, NULL, lineno ); }
	| '{' statement_list '}'
          { $$ = makedouble ( Ncompound, NULL, $2, lineno ); }
	| '{' declaration_list '}'
          { $$ = makedouble ( Ncompound, $2, NULL, lineno ); }
	| '{' declaration_list statement_list '}'
          { $$ = makedouble ( Ncompound, $2, $3, lineno ); }
	;

declaration_list
        : declaration_list_l
          { $$ = $1.head; }

declaration_list_l
	: declaration
          { $$ = makelist ( empty_list, makedouble ( Ndecllist, $1, NULL, lineno ), lineno ); }
	| declaration_list_l declaration
          { $$ = makelist ( $1, makedouble ( Ndecllist, $2, NULL, lineno ), lineno ); }
	;

statement_list
        : statement_list_l
          { $$ = $1.head; }

statement_list_l
	: statement
          { $$ = makelist ( empty_list, makedouble ( Nstmtlist, $1, NULL, lineno ), lineno ); }
	| statement_list_l statement
          { $$ = makelist ( $1, makedouble ( Nstmtlist, $2, NULL, lineno ), lineno ); }
	;

expression_statement
	: ';'
          { $$ = NULL; }
	| expr ';'
          { $$ = $1; }
	;

selection_statement
	: IF '(' expr ')' statement
          { $$ = maketriple (Nif, $3, $5, NULL, lineno); }
	| IF '(' expr ')' statement ELSE statement
          { $$ = maketriple (Nif, $3, $5, $7, lineno); }
	| SWITCH '(' expr ')' statement
          { $$ = errornode (lineno); }
	;

iteration_statement
	: WHILE '(' expr ')' statement
          { $$ = makedouble (Nwhile, $3, $5, lineno); }
	| DO statement WHILE '(' expr ')' ';'
          { node * whileNode = makedouble(Nwhile, $5, $2, lineno);
            node * stmtNode = makedouble(Nstmtlist, whileNode, NULL, lineno);
            $$ = makedouble(Nstmtlist, $2, stmtNode, lineno);
          }
	| FOR '(' ';' ';' ')' statement
          { node * loopNode = makeconst(1,0,lineno);
            $$ = makedouble (Nwhile, loopNode, $6, lineno); 
          }
	| FOR '(' ';' ';' expr ')' statement
          { node * loopNode = makeconst(1,0,lineno);
            node * stmtNode = makedouble(Nstmtlist, $5, NULL, lineno);
            node * stmtNode2 = makedouble(Nstmtlist, $7, stmtNode, lineno);
            $$ = makedouble(Nwhile, loopNode, stmtNode2, lineno); 
          }
	| FOR '(' ';' expr ';' ')' statement
          { $$ = makedouble (Nwhile, $4, $7, lineno); }
	| FOR '(' ';' expr ';' expr ')' statement
          { node * stmtNode = makedouble(Nstmtlist, $6, NULL, lineno);
            node * stmtNode2 = makedouble(Nstmtlist, $8, stmtNode, lineno);
            $$ = makedouble(Nwhile, $4, stmtNode2, lineno); 
          }
	| FOR '(' expr ';' ';' ')' statement
          { node * loopNode = makeconst(1,0,lineno);
            node * whileNode = makedouble(Nwhile, loopNode, $7, lineno);
            node * stmtNode = makedouble(Nstmtlist, whileNode, NULL, lineno);
            $$ = makedouble(Nstmtlist, $3, stmtNode, lineno);
          }
	| FOR '(' expr ';' ';' expr ')' statement
          { node * loopNode = makeconst(1,0,lineno);
            node * stmtNode = makedouble(Nstmtlist, $6, NULL, lineno);
            node * stmtNode2 = makedouble(Nstmtlist, $8, stmtNode, lineno);
            node * whileNode = makedouble(Nwhile, loopNode, stmtNode2, lineno);
            node * stmtNode3 = makedouble(Nstmtlist, whileNode, NULL, lineno);
            $$ = makedouble(Nstmtlist, $3, stmtNode3, lineno); 
          }
	| FOR '(' expr ';' expr ';' ')' statement
          { node * whileNode = makedouble(Nwhile, $5, $8, lineno);
            node * stmtNode = makedouble(Nstmtlist, whileNode, NULL, lineno);
            $$ = makedouble(Nstmtlist, $3, stmtNode, lineno);
          }
        | FOR '(' expr ';' expr ';' expr ')' statement
          { node * stmtNode = makedouble(Nstmtlist, $7, NULL, lineno);
            node * stmtNode2 = makedouble(Nstmtlist, $9, stmtNode, lineno);
            node * whileNode = makedouble(Nwhile, $5, stmtNode2, lineno);
            node * stmtNode3 = makedouble(Nstmtlist, whileNode, NULL, lineno);
            $$ = makedouble(Nstmtlist, $3, stmtNode3, lineno); 
          }
	;

jump_statement
	: GOTO identifier ';'
          { $$ = errornode (lineno); }
	| CONTINUE ';'
          { $$ = makesingle(Ncontinue, NULL, lineno); }
	| BREAK ';'
          { $$ = makesingle(Nbreak, NULL, lineno); }
	| RETURN ';'
          { $$ = makesingle(Nreturn, NULL, lineno); }
	| RETURN expr ';'
          { $$ = makesingle(Nreturn, $2, lineno); }
	;

file    : file_l
          { syntaxtree = $1.head; }
        ;

file_l
	: external_definition
	  { printd("external_definition\n");
            $$ = makelist(empty_list, makedouble( Ndefnlist, $1, NULL, lineno ), lineno); }
	| file_l external_definition
	  { printd("file_l external_definition\n");
	    $$ = makelist( $1, makedouble( Ndefnlist, $2, NULL, lineno ), lineno ); }
	;

external_definition
	: function_definition
	  { $$ = $1; }
	| declaration
          { $$ = $1; }
	;

function_definition
	: declarator function_body
	  { printd("declarator function_body\n");
            $$ = maketriple ( Nfuncdefn, makeident(lookup("int"), lineno), $1, $2, lineno );
          }
	| declaration_specifiers declarator function_body
          { printd("declaration_specifiers declarator function_body\n");
            $$ = maketriple ( Nfuncdefn, $1, $2, $3, lineno );
          }
	;

function_body
	: compound_statement
	  { $$ = $1; }
	| declaration_list compound_statement
	  { $$ = errornode (lineno); }
	;

identifier
	: IDENTIFIER
	  { $$ = makeident ( yylval.lexeme.identptr, lineno ); }
	;
%%

#include <stdio.h>

#include "syntaxtree.h"

#include "y.tab.h"

extern char yytext[];
extern int column;
extern int lineno;

/* THE syntax tree */
extern node * syntaxtree;

yyerror(s)
char *s;
{
	fflush(stdout);
	printf("\n%*s\n%*s\n", column, "^", column, s);
	printf("at line %d\n", lineno);
}
