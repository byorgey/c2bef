/* status_code.h

   Standard status codes
   
   C-to-Befunge translation project
   
   Steve Winslow
   Brent Yorgey
*/

#ifndef __STATUSCODE_H
#define __STATUSCODE_H

typedef enum status_code { OK = 0, failure, underflow, overflow, not_found, out_of_bounds, ugly_user_error } status_code;

#endif
