/* consttab.c

   Constant table implementation.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#include "consttab.h"
#include "befgen.h"

/* THE constant table. */
extern DLL const_table;

void init_const_table ()
{
  const_table = DLL_Create();
}

void destroy_const_table ()
{
  DLL_Destroy ( const_table );
}

void print_ct_entry ( void * ctv )
{
  ct_entry * ct = (ct_entry *) ctv;

  printf ("x: %d   y: %d   value: %d\n", ct->x, ct->y, ct->value);
}

void add_constant ( vs * v, int xpos, int ypos, long int val )
{
  ct_entry * cte = (ct_entry *) malloc ( sizeof(ct_entry) );
  cte->vscr = v;
  cte->x = xpos;
  cte->y = ypos;
  cte->value = val;
  
  cte->type = STATIC;
  cte->st_entry = NULL;

  DLL_InsertBack ( const_table, cte );
}

void add_dynamic_constant ( vs * v, int xpos, int ypos, const_type t, dcldesc * st )
{
  ct_entry * cte = (ct_entry *) malloc ( sizeof(ct_entry) );
  cte->vscr = v;
  cte->x = xpos;
  cte->y = ypos;
  
  cte->type = t;
  cte->st_entry = st;

  DLL_InsertBack ( const_table, cte );
}

/* Add an entry to the constant table to deal with relative jumps. */
void add_reljump_constant ( vs * v, int x, int y, int jump_loc,
							const_type t, dcldesc * st ) {
  ct_entry * cte = (ct_entry *) malloc ( sizeof(ct_entry) );
  cte->vscr = v;
  cte->x = x;
  cte->y = y;

  /* temporarily store the jump-from location in value;
	 calculate_constants() will figure it out later */
  cte->value = jump_loc;
  
  cte->type = t;
  cte->st_entry = st;

  DLL_InsertBack ( const_table, cte );
}

void calculate_constants ()
{
  long location;
  ct_entry * c;

  for ( DLL_IterBegin ( const_table );
	!DLL_IterAtEnd ( const_table );
	DLL_IterNext ( const_table )    ) {

    c = (ct_entry *) DLL_IterCurrent ( const_table );

    switch ( c->type ) {
    case STATIC:
      /* Do nothing: constant already has a (static) value */
      break;

    case FUNCLOC_X:
	  location =
		c -> st_entry -> func.entry_vs -> global_x +
		c -> st_entry -> func.entry_pos -> pos_x -
		c -> vscr -> global_x;
	  c -> value = location - (c -> value);
      break;

	case FUNCLOC_Y:
	  location =
		c -> st_entry -> func.entry_vs -> global_y +
		c -> st_entry -> func.entry_pos -> pos_y -
		c -> vscr -> global_y;
	  c -> value = location - (c -> value);
      break;

    default:
      printf( "BAD WEIRDNESS: unrecognized const_type encountered in calculate_constants()\n" );
      exit(1);

    }
  }
}

vs * generate_flinger ()
{
  vs * tempVS = vs_createStandard();
  vs_pos * temp = create_vs_pos();

  long x, y;

  ct_entry * curEntry = NULL;
  
  char * constString = NULL;

  calculate_constants();

  vs_putString(tempVS, "01-", temp, WRAP);
  vs_putString(tempVS, SP, temp, WRAP);
  vs_putString(tempVS, "p", temp, WRAP);
  
  for (DLL_IterBegin(const_table);
       !DLL_IterAtEnd(const_table);
       DLL_IterNext(const_table))   {

    curEntry = (ct_entry *)DLL_IterCurrent(const_table);
    
    constString = num2befrep(curEntry -> value);
    vs_putString(tempVS, constString, temp, WRAP);
    free(constString);
    constString = num2befrep(curEntry -> x + curEntry -> vscr -> global_x);
    vs_putString(tempVS, constString, temp, WRAP);
    free(constString);
    constString = num2befrep(curEntry -> y + curEntry -> vscr -> global_y);
    vs_putString(tempVS, constString, temp, WRAP);
    free(constString);
    vs_putString(tempVS, "p", temp, WRAP);
  }

  /* now, put code to jump back up to main function
	 we'll start the actual code on a new line, going to the right */

  if (temp -> dir == RIGHT) {
	vs_putChar(tempVS, 'v', VS_WIDTH - 1, temp -> pos_y);
	vs_putChar(tempVS, '<', VS_WIDTH - 1, temp -> pos_y + 1);
	set_vs_pos(temp, VS_WIDTH - 2, temp -> pos_y + 1, LEFT);
  }

  vs_putChar(tempVS, 'v', 0, temp -> pos_y);
  vs_putChar(tempVS, '>', 0, temp -> pos_y + 1);
  vs_putChar(tempVS, 'x', VS_WIDTH - 2, temp -> pos_y + 1);
  vs_putChar(tempVS, '@', VS_WIDTH - 1, temp -> pos_y + 1);
  set_vs_pos(temp, 1, temp -> pos_y + 1, RIGHT);

  constString = num2befrep(-(temp -> pos_y));
  vs_putString(tempVS, "1", temp, WRAP);
  vs_putString(tempVS, constString, temp, WRAP);
  vs_putString(tempVS, "2", temp, WRAP);
  vs_putString(tempVS, constString, temp, WRAP);
  free(constString);

  return tempVS;
}
