#include "symtab.h"
#include "syntreeops.h"
#include "consttab.h"

#include "vs_algo.h"

#define DEBUG 1

/* THE syntax tree */
node * syntaxtree = NULL;

/* THE identifier descriptor hash */
Hash idHash;

/* THE symbol table */
DLL symboltable;

/* THE constant table */
DLL const_table;

/* empty list "constant" */
struct listhead empty_list;

/* THE big list of gloms. */
DLL vs_list;


FILE * fp;
vs * glom, * flinger;

void Initialize () 
{
  idHash = Hash_Create(stringHash, identdescMatches);
  insert_special_hash_entries();
  empty_list.head = empty_list.tail = NULL;

  init_const_table();

  vs_list = DLL_Create();
}

void Cleanup () 
{
  destroy_glom ( glom );
  DLL_MakeEmpty ( vs_list );
  DLL_Destroy ( vs_list );

  destroy_const_table();

  fclose(fp);
}

int main( int argc, char * argv[] ) {

  char * n;

  /* FIX THIS ??? */
  if ( argc < 2 ) {
    printf("Arg!!\n");
    exit(1);
  } 

  Initialize();

  fp = fopen(argv[1],"w");

  yyparse();
  collapsetree(syntaxtree);
  
#if DEBUG
  printsyntree(syntaxtree, 0);
#endif

  build_symbol_table ();

#if DEBUG
  DLL_Traverse ( symboltable, printdcldesc_wrapper );
#endif

  /* Generate code. Stick all the resulting vs gloms into the vs_list,
     with the code for main() as the very first item in the list. */

  gen_code ( syntaxtree );

  /* Link together all the individual gloms */
  glom = glomify ( vs_list );

  /* Fill out the glom so that it's a regular rectangular
     doubly-linked structure with no holes, by putting special "empty"
     vs's where appropriate. Also, set the global_x and global_y
     fields appropriately for each vs. 
  */
  fill_blanks ( glom );

  /* Generate the constant flinger. */
  flinger = generate_flinger ();

#if DEBUG
  DLL_Traverse ( const_table, print_ct_entry );
#endif

  /* Attach the constant flinger to the rest of the code. */
  vs_setVS ( flinger, glom, RIGHT );
  glom = flinger;

  /* Now call fill_blanks again so the output function works correctly. */
  fill_blanks ( glom );

  /* Write out the final code! */
  vs_outputLinkedScreens ( glom , fp ); 

  /* Now clean up after ourselves. */
  Cleanup ();

  return 0;
}
