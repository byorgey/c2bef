/* hash.c

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#include "hash.h"

Hash Hash_Create ( int (*hash_f)( Item_ptr key ), 
		   bool (*isEq)(Item_ptr item1, Item_ptr item2) )
{
  Hash new_hash = (Hash) malloc(sizeof(Hash_struct));

  int i;
  for ( i = 0; i < HASH_SIZE; i++ ) {
    new_hash->table[i] = DLL_Create();
  }

  new_hash->num_elts = 0;

  new_hash->hash_func = hash_f;
  new_hash->isEqual = isEq;

  return new_hash;
}


status_code Hash_Clear ( Hash h ) 
{
  int i;
  for ( i = 0; i < HASH_SIZE; i++) {
    DLL_Clear ( h->table[i] );
  }

  h->num_elts = 0;
 
  return OK;
}


status_code Hash_Destroy ( Hash h )
{
  status_code status = Hash_Clear ( h );

  if ( status == OK )
    free(h);

  return status;
}


void Hash_Insert ( Hash h, Item_ptr item, Item_ptr key )
{
  int hash_loc = (*(h->hash_func))(key) % HASH_SIZE;

  DLL_InsertBack ( h->table[hash_loc], item );
}


Item_ptr Hash_Lookup ( Hash h, Item_ptr key )
{
  int hash_loc = (*(h->hash_func))(key) % HASH_SIZE;

  return DLL_Select ( h->table[hash_loc], key, h->isEqual );
}


void Hash_Traverse ( Hash h, void (*ProcessItem) (Item_ptr item) )
{
  int i;
  for ( i = 0; i < HASH_SIZE; i++ ) {
    DLL_Traverse ( h->table[i], ProcessItem );
  }
}

