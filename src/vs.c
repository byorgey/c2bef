/* vs.c

   Data structure for "virtual screens" of Funge-space.
   (The term "virtual screens" is preserved for
   historical reasons.)

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#include "vs.h"

/* creates a new virtual screen and initializes all cells to ASCII 32. */
vs * vs_createStandard() {
  return vs_create(VS_WIDTH, VS_HEIGHT);
}

vs * vs_create(int x, int y) {
  int i, j;
  vs * tempVS = NULL;

  if (x < 5 || y < 5)
	return NULL;
  
  tempVS = (vs *)malloc(sizeof(vs));
  tempVS -> space = (char * *)malloc(sizeof(char *) * x);

  for (i = 0; i < x; i++)
	tempVS -> space[i] = (char *)malloc(sizeof(char) * y);
  
  
  for (i = 0; i < x; i++)
	for (j = 0; j < y; j++)
	  tempVS -> space[i][j] = ' ';

  tempVS -> screen_x = x;
  tempVS -> screen_y = y;
  
  tempVS -> leftscreen = NULL;
  tempVS -> rightscreen = NULL;
  tempVS -> upscreen = NULL;
  tempVS -> downscreen = NULL;

  tempVS -> global_x = 0;
  tempVS -> global_y = 0;

  tempVS -> maxright = -1;
  tempVS -> maxdown = -1;
  tempVS -> visited = 0;
  
  return tempVS;
}

vs * vs_createEmpty() {
  int i, j;
  vs * tempVS = NULL;

  tempVS = (vs *)malloc(sizeof(vs));
  tempVS -> space = NULL;

  tempVS -> screen_x = 0;
  tempVS -> screen_y = 0;
  
  tempVS -> leftscreen = NULL;
  tempVS -> rightscreen = NULL;
  tempVS -> upscreen = NULL;
  tempVS -> downscreen = NULL;

  tempVS -> global_x = 0;
  tempVS -> global_y = 0;

  tempVS -> maxright = -1;
  tempVS -> maxdown = -1;
  tempVS -> visited = 0;
  
  return tempVS;
}

void vs_destroy(vs * screen) {
  if (screen == NULL) return;
  if (screen -> space != NULL)
	free(screen -> space);
  free(screen);
}


/* set screen's neighbor screen in direction dir to screen2 */
/* return screen (creates one if screen is NULL) */
vs * vs_setVS(vs * screen, vs * screen2, direction_t dir) {
  vs * currentVS = screen;
  if (currentVS == NULL)
	currentVS = vs_createStandard();
  switch (dir) {
  case LEFT:
	currentVS -> leftscreen = screen2;
	screen2 -> rightscreen = currentVS;
	break;
  case RIGHT:
	currentVS -> rightscreen = screen2;
	screen2 -> leftscreen = currentVS;
	break;
  case UP:
	currentVS -> upscreen = screen2;
	screen2 -> downscreen = currentVS;
	break;
  case DOWN:
	currentVS -> downscreen = screen2;
	screen2 -> upscreen = currentVS;
	break;
  }
  return currentVS;
}
	

/* get pointer to linked screens */
vs * vs_getNeighbor(vs * screen, direction_t dir) {
  if (screen == NULL) {
	printf("BAD BAD: calling getNeighbor() on a NULL screen.\n");
	exit(1);
  }
  switch (dir) {
  case LEFT:
	return screen -> leftscreen;
  case RIGHT:
	return screen -> rightscreen;
  case UP:
	return screen -> upscreen;
  case DOWN:
	return screen -> downscreen;
  }
  printf("BAD WEIRDNESS: calling getNeighbor with a bad direction_t (%d).\n", dir);
  exit(1);
}

/* get pointer to linked screens; create them if they don't exist */

vs * vs_createNeighbor(vs * screen, direction_t dir) {
  if (screen == NULL) {
	printf("BAD BAD: calling createNeighbor() on a NULL screen.\n");
	exit(1);
  }
  switch (dir) {
  case LEFT:
	if (screen -> leftscreen == NULL) {
	  screen -> leftscreen = vs_createStandard();
	  screen -> leftscreen -> rightscreen = screen;
	}
	return screen -> leftscreen;
  case RIGHT:
	if (screen -> rightscreen == NULL) {
	  screen -> rightscreen = vs_createStandard();
	  screen -> rightscreen -> leftscreen = screen;
	}
	return screen -> rightscreen;
  case UP:
	if (screen -> upscreen == NULL) {
	  screen -> upscreen = vs_createStandard();
	  screen -> upscreen -> downscreen = screen;
	}
	return screen -> upscreen;
  case DOWN:
	if (screen -> downscreen == NULL) {
	  screen -> downscreen = vs_createStandard();
	  screen -> downscreen -> upscreen = screen;
	}
	return screen -> downscreen;
  }
  printf("BAD WEIRDNESS: calling createNeighbor with a bad direction_t (%d).\n", dir);
  exit(1);
}

/* print contents of vs to standard output */
void vs_printScreen(vs * screen, int printBorder) {
  int i, j;

  printf("x = %d, y = %d\n", screen -> screen_x, screen -> screen_y);
  
  if (printBorder == 1) {
	printf(" ");
	for (i = 0; i < screen -> screen_x; i++) {
	  if (i % 5 == 0)
		printf("|");
	  else
		printf("-");
	}
	printf("\n");
  }

  for (j = 0; j < screen -> screen_y; j++) {
	if (printBorder == 1) {
	  if (j % 5 == 0)
		printf("-");
	  else
		printf("|");
	}

	for (i = 0; i < screen -> screen_x; i++) {
	  printf("%c",vs_getChar(screen, i, j));
	  fflush(stdout);
	}

	if (printBorder == 1) {
	  if (j % 5 == 0)
		printf("-");
	  else
		printf("|");
	}
	printf("\n");
  }
  if (printBorder == 1) {
	printf(" ");
	for (i = 0; i < screen -> screen_x; i++) {
	  if (i % 5 == 0)
		printf("|");
	  else
		printf("-");
	}
  }
  printf("\n");
}


/* print one line of given funge-space. */
void vs_outputLine(vs * screen, int lineno, FILE * fp) {
  int i;
  if (screen -> space == NULL) {
	/* empty screen; just print blank space */
	for (i = 0; i < screen -> screen_x; i++) fprintf(fp, " ");
  } else {
	for (i = 0; i < screen -> screen_x; i++)
	  fprintf(fp, "%c", screen -> space[i][lineno]);
  }
}  

/* print all screens linked, starting with this one as the upper left. */
void vs_outputLinkedScreens(vs * screen, FILE * fp) {
  int i, j;
  vs * iterator = screen;
  vs * iterator2 = screen;
  while (iterator != NULL) {
	for (i = 0; i < iterator -> screen_y; i++) {
	  iterator2 = iterator;
	  while (iterator2 != NULL) {
		vs_outputLine(iterator2, i, fp);
		iterator2 = iterator2 -> rightscreen;
	  }
	  fprintf(fp, "\n");
	}
	iterator = iterator -> downscreen;
  }
}

/* print all screens to standard output */
void vs_printLinkedScreens(vs * screen) {
  vs_outputLinkedScreens(screen, stdout);
}

/* return a pointer to the vs in which the given cell resides.
   if create is on, create a new vs to hold it.
   if change_pos == 1, change contents of x & y. */
vs * vs_findScreen(vs * screen, int * px, int * py, int create, int change_pos) {
  vs * currentVS = screen;
  int x = *px;
  int y = *py;
  
  if (currentVS == NULL)
	return NULL;
  
  while (x < 0 || (x >= currentVS -> screen_x) || y < 0 || (y >= currentVS -> screen_y)) {
	if (x < 0) {
	  /* current screen is too far to the right; move left */
	  /* printf("Moving left...\n"); */
	  if (create == 0) {
		currentVS = vs_getNeighbor(currentVS, LEFT);
		if (currentVS == NULL) {
		  return NULL;
		}
		x += currentVS -> screen_x;
	  } else {
		currentVS = vs_createNeighbor(currentVS, LEFT);
		x += currentVS -> screen_x;
	  }
	}
	if (x >= currentVS -> screen_x) {
	  /* current screen is too far to the left; move right */
	  /* printf("Moving right...\n"); */
	  if (create == 0) {
		currentVS = vs_getNeighbor(currentVS, RIGHT);
		if (currentVS == NULL) {
		  return NULL;
		}
		x -= currentVS -> screen_x;
	  } else {
		currentVS = vs_createNeighbor(currentVS, RIGHT);
		x -= currentVS -> screen_x;
	  }
	}
	if (y < 0) {
	  /* current screen is too far down; move up */
	  /* printf("Moving up...\n"); */
	  if (create == 0) {
		currentVS = vs_getNeighbor(currentVS, UP);
		if (currentVS == NULL) {
		  return NULL;
		}
		y += currentVS -> screen_y;
	  } else {
		currentVS = vs_createNeighbor(currentVS, UP);
		y += currentVS -> screen_y;
	  }
	}
	if (y >= currentVS -> screen_y) {
	  /* current screen is too far up; move down */
	  /* printf("Moving down...\n"); */
	  if (create == 0) {
		currentVS = vs_getNeighbor(currentVS, DOWN);
		if (currentVS == NULL) {
		  return NULL;
		}
		y -= currentVS -> screen_y;	
	  } else {
		currentVS = vs_createNeighbor(currentVS, DOWN);
		y -= currentVS -> screen_y;
	  }
	}
  }
  if (change_pos == 1) {
	*px = x;
	*py = y;
  }
  return currentVS;
}


/* return pointer to actual char cell of this x,y position */
/* return NULL on out-of-bounds error */
char * vs_findCell(vs * screen, int x, int y, int create) {
  vs * currentVS = vs_findScreen(screen, &x, &y, create, 1);
  if (currentVS == NULL)
	return NULL;
  return &(currentVS -> space[x][y]);
}


/* get contents of this cell */
/* return -1 on out-of-bounds error */
char vs_getChar(vs * screen, int x, int y) {
  char * ptrCell = vs_findCell(screen, x, y, 0);
  if (ptrCell == NULL)
	return -1;
  return *ptrCell;
}

/* vs_putChar will overwrite the current value of the cell
   with extreme prejudice. */
/* return -1 on out-of-bounds error */
void vs_putChar(vs * screen, char c, int x, int y) {
  char * ptrCell = vs_findCell(screen, x, y, 1);
  *ptrCell = c;
}

/* positions the cursor at a newline */
void vs_newline(vs * screen, vs_pos * temp) {
  int x = temp -> pos_x;
  int y = temp -> pos_y;
  direction_t dir = temp -> dir;
  if (dir == RIGHT) {
	if (x < temp -> bound_right)
	  x = temp -> bound_right;
	vs_putChar(screen, 'v', x, y);
	y++;
	vs_putChar(screen, '<', x, y);
	x--;
  }
  x = temp -> bound_left;
  vs_putChar(screen, 'v', x, y);
  y++;
  vs_putChar(screen, '>', x, y);
  x++;
  temp -> pos_x = x;
  temp -> pos_y = y;
  temp -> dir = RIGHT;
}

void vs_enilwen(vs * screen, vs_pos * temp) {
  int x = temp -> pos_x;
  int y = temp -> pos_y;
  direction_t dir = temp -> dir;
  if (dir == LEFT) {
	if (x < temp -> bound_left)
	  x = temp -> bound_left;
	vs_putChar(screen, 'v', x, y);
	y++;
	vs_putChar(screen, '>', x, y);
	x++;
  }
  x = temp -> bound_right;
  vs_putChar(screen, 'v', x, y);
  y++;
  vs_putChar(screen, '<', x, y);
  x--;
  temp -> pos_x = x;
  temp -> pos_y = y;
  temp -> dir = LEFT;
}

/* put string headed in this direction, starting with given cell */
/* ??? NOTE: wrapping will overwrite on screen edges, regardless of
   overwrite setting */
/* pass 1 to wrap to enable wrapping */
/* pass 1 to overwrite to replace char's instead of checking to see
   if they're already filled */
/* return -1 on error */
int vs_putString(vs * screen, char * st, vs_pos * currentPos, vs_outputType_t wrap) {
  vs * curScreen;
  char * iterator = st;
  int x = currentPos -> pos_x;
  int y = currentPos -> pos_y;
  int bound_distance;
  int count = 0;
  int len = strlen(st);
  direction_t dir = currentPos -> dir;

  if (screen == NULL) {
	printf("BAD BAD: Trying to write to an empty screen (vs_putString).\n");
	exit(1);
  }

  if (wrap == CONTIGUOUS) {
	/* need to check and make sure we have enough space to output string
	   in its entirety.  if we don't, output spaces until we do. */
	/* if the string is too long to possibly fit in a single row,
	   make sure it NOWRAP's to the right. */
	if (len > (currentPos -> bound_right - currentPos -> bound_left - 1)) {
	  /* string is too long to fit on one line; newline to the left boundary
		 and NOWRAP it to the right */
	  vs_newline(screen, currentPos);
	  x = currentPos -> pos_x;
	  y = currentPos -> pos_y;
	  dir = currentPos -> dir;
	} else {
	  /* the string will fit in a single line.  now we have to make sure
		 that this given line has enough space. */
	  if (dir == LEFT) {
		bound_distance = x - (currentPos -> bound_left);
		if (len > bound_distance) {
		  /* need to wrap before we write the string */
		  if (x > currentPos -> bound_left)
			x = currentPos -> bound_left;
		  vs_putChar(screen, 'v', x, y);
		  y++;
		  vs_putChar(screen, '>', x, y);
		  x++;
		  dir = RIGHT;
		}
	  } else if (dir == RIGHT) {
		bound_distance = (currentPos -> bound_right) - x;
		if (len > bound_distance) {
		  /* need to wrap before we write the string */
		  if (x < currentPos -> bound_right)
			x = currentPos -> bound_right;
		  vs_putChar(screen, 'v', x, y);
		  y++;
		  vs_putChar(screen, '<', x, y);
		  x--;
		  dir = LEFT;
		}
	  }
	}
  }
	
	
  while (*iterator != '\0') {
	curScreen = vs_findScreen(screen, &x, &y, 1, 0);

	if (wrap == WRAP) {
	  if ((dir == LEFT) &&
		  (x % screen -> screen_x == currentPos -> bound_left)) {
		vs_putChar(screen, 'v', x, y);
		y++;
		vs_putChar(screen, '>', x, y);
		x++;
		dir = RIGHT;
	  }
	  if ((dir == RIGHT) &&
		  (x % screen -> screen_x == currentPos -> bound_right)) {
		vs_putChar(screen, 'v', x, y);
		y++;
		vs_putChar(screen, '<', x, y);
		x--;
		dir = LEFT;
	  }
	  if ((dir == UP) &&
		  (y % screen -> screen_y == currentPos -> bound_up)) {
		vs_putChar(screen, '>', x, y);
		x++;
		vs_putChar(screen, 'v', x, y);
		y++;
		dir = DOWN;
	  }
	  if ((dir == DOWN) &&
		  (y % screen -> screen_y == currentPos -> bound_down)) {
		vs_putChar(screen, '>', x, y);
		x++;
		vs_putChar(screen, '^', x, y);
		y--;
		dir = UP;
	  }
	}

	vs_putChar(screen, *iterator, x, y);
	count++;
	
	/* update x & y positions */
	switch (dir) {
	case LEFT:
	  x--;
	  break;
	case RIGHT:
	  x++;
	  break;
	case UP:
	  y--;
	  break;
	case DOWN:
	  y++;
	  break;
	default:
	  printf("BAD WEIRDNESS: incorrect direction_t in putString (%d)\n", dir);
	  exit(1);
	}

	iterator++;
  }

  currentPos -> pos_x = x;
  currentPos -> pos_y = y;
  currentPos -> dir = dir;

  if (count != len)
	printf("BAD WEIRDNESS: printing too many characters. (count = %d, len = %d, str = %s)\n", count, len, st);
  
  return 0;
}


/* putStringStringmode - adds quotes as needed */
int vs_putStringStringmode(vs * screen, char * st, vs_pos * currentPos,
						   int wrap, int overwrite) {
  char * tempStr = (char *)malloc(sizeof(char) * (strlen(st) + 3));
  char * iterator = tempStr;
  vs * curScreen = NULL;
  int x = currentPos -> pos_x;
  int y = currentPos -> pos_y;
  direction_t dir = currentPos -> dir;
  if (screen == NULL) {
	printf("BAD BAD: Trying to write to an empty screen (vs_putString).\n");
	exit(1);
  }

  sprintf(tempStr, "\"%s\"", st);

  if (wrap == WRAP) {
	curScreen = vs_findScreen(screen, &x, &y, 1, 0);
	if ((dir == LEFT) &&
		(x % screen -> screen_x <= currentPos -> bound_left + 1)) {
	  vs_putChar(screen, 'v', x, y);
	  y++;
	  vs_putChar(screen, '>', x, y);
	  x++;
	  dir = RIGHT;
	}
	if ((dir == RIGHT) &&
		(x % screen -> screen_x >= currentPos -> bound_right - 1)) {
	  vs_putChar(screen, 'v', x, y);
	  y++;
	  vs_putChar(screen, '<', x, y);
	  x--;
	  dir = LEFT;
	}
	if ((dir == UP) &&
		(y % screen -> screen_y <= currentPos -> bound_up + 1)) {
	  vs_putChar(screen, '>', x, y);
	  x++;
	  vs_putChar(screen, 'v', x, y);
	  y++;
	  dir = DOWN;
	}
	if ((dir == DOWN) &&
		(y % screen -> screen_y >= currentPos -> bound_down - 1)) {
	  vs_putChar(screen, '>', x, y);
	  x++;
	  vs_putChar(screen, '^', x, y);
	  y--;
	  dir = UP;
	}
  }
		
  
  while (*iterator != '\0') {
	curScreen = vs_findScreen(screen, &x, &y, 1, 0);
	if (wrap == WRAP) {
	  if ((dir == LEFT) &&
		  (x % screen -> screen_x == currentPos -> bound_left + 1)) {
		vs_putChar(screen, '"', x, y);
		x--;
		vs_putChar(screen, 'v', x, y);
		y++;
		vs_putChar(screen, '>', x, y);
		x++;
		vs_putChar(screen, '"', x, y);
		x++;
		dir = RIGHT;
	  }
	  if ((dir == RIGHT) &&
		  (x % screen -> screen_x == currentPos -> bound_right - 1)) {
		vs_putChar(screen, '"', x, y);
		x++;
		vs_putChar(screen, 'v', x, y);
		y++;
		vs_putChar(screen, '<', x, y);
		x--;
		vs_putChar(screen, '"', x, y);
		x--;
		dir = LEFT;
	  }
	  if ((dir == UP) &&
		  (y % screen -> screen_y == currentPos -> bound_up + 1)) {
		vs_putChar(screen, '"', x, y);
		y--;
		vs_putChar(screen, '>', x, y);
		x++;
		vs_putChar(screen, 'v', x, y);
		y++;
		vs_putChar(screen, '"', x, y);
		y++;
		dir = DOWN;
	  }
	  if ((dir == DOWN) &&
		  (y % screen -> screen_y == currentPos -> bound_down - 1)) {
		vs_putChar(screen, '"', x, y);
		y++;
		vs_putChar(screen, '>', x, y);
		x++;
		vs_putChar(screen, '^', x, y);
		y--;
		vs_putChar(screen, '"', x, y);
		y--;
		dir = UP;
	  }
	}

	vs_putChar(screen, *iterator, x, y);

	/* update x & y positions */
	switch (dir) {
	case LEFT:
	  x--;
	  break;
	case RIGHT:
	  x++;
	  break;
	case UP:
	  y--;
	  break;
	case DOWN:
	  y++;
	  break;
	default:
	  printf("BAD WEIRDNESS: incorrect direction_t in putString (%d)\n", dir);
	  exit(1);
	}

	iterator++;
  }

  currentPos -> pos_x = x;
  currentPos -> pos_y = y;
  currentPos -> dir = dir;

  free(tempStr);
  
  return 0;
}



/**************************************
 *  Used by vs internal functions.    *
 *  Don't even think about trying to  *
 *    use these in any other way.     *
 **************************************/


/* return the x-value of the current vs right boundary in "world" coords
   based on the current vs_pos */
int vs_screenRightBoundary(vs * screen, vs_pos * curPosition) {
  int x;
  for (x = 0; x < curPosition -> pos_x; x += screen -> screen_x);
  return x - 1;
}

/* return the y-value of the current vs down boundary in "world" coords
   based on the current vs_pos */
int vs_screenDownBoundary(vs * screen, vs_pos * curPosition) {
  int y;
  for (y = 0; y < curPosition -> pos_y; y += screen -> screen_y);
  return y - 1;
}



/******************
 *  vs_pos stuff  *
 ******************/


/* create a vs_pos */
vs_pos * create_vs_pos() {
  vs_pos * temp_vs_pos = (vs_pos *)malloc(sizeof(vs_pos));

  temp_vs_pos -> pos_x = 0;
  temp_vs_pos -> pos_y = 0;
  temp_vs_pos -> dir = RIGHT;

  temp_vs_pos -> bound_left = 0;
  temp_vs_pos -> bound_right = VS_WIDTH - 1;
  temp_vs_pos -> bound_up = 0;
  temp_vs_pos -> bound_down = VS_HEIGHT - 1;

  return temp_vs_pos;
}

/* set value of vs_pos elements */
void set_vs_pos(vs_pos * temp, int x, int y, direction_t dir) {
  temp -> pos_x = x;
  temp -> pos_y = y;
  temp -> dir = dir;
}

/* set left and right boundaries of vs_pos */
void set_vs_pos_boundaries(vs_pos * temp, int left, int right, int up, int down) {
  temp -> bound_left = left;
  temp -> bound_right = right;
  temp -> bound_up = up;
  temp -> bound_down = down;
}
  


/***************************************
 *  utility funcs (you may use these)  *
 ***************************************/

/* build a path from the current position to the lower left or
   lower right corner */
/* return -1 if unable to build path without destroying data */
int vs_buildExitPath(vs * screen, vs_pos * curPosition) {
  int i, j;
  char c;
  int flag = 0;
  int x = curPosition -> pos_x;
  int y = curPosition -> pos_y;
  int dir = curPosition -> dir;

  int exit_x = vs_screenRightBoundary(screen, curPosition);
  int exit_y = vs_screenDownBoundary(screen, curPosition);

  c = vs_getChar(screen, x, y);
  
  /* if one of these fails, we can't exit without overwriting code */
  if ((c = vs_getChar(screen, x, y)) != ' ') {
	return -1;
  }
  if ((c = vs_getChar(screen, exit_x, exit_y)) != ' ') {
	return -1;
  }
  
  /* now check to make sure there's nothing between us
	 and the right boundary */
  for (i = x; i < exit_x; i++) {
	if ((c = vs_getChar(screen, i, y)) != ' ') {
	  flag = 1;
	  break;
	}
  }

  if (flag == 0) {
	/* can walk to the right edge without hitting anything;
	   now test bottom */
	for (j = y; j < exit_y; j++) {
	  if ((c = vs_getChar(screen, exit_x, j)) != ' ') {
		flag = 1;
		break;
	  }
	}
	if (flag == 0) {
	  /* safe; make path right, then down */
	  vs_putChar(screen, '>', x, y);
	  vs_putChar(screen, 'v', exit_x, y);
	  set_vs_pos(curPosition, exit_x, exit_y, DOWN);
	  return 0;
	}
  }

  /* if we're here, there was a problem either going right, or
	 right then down */
  /* try down, then right */

  flag = 0;
  
  for (j = y; j < exit_y; j++) {
	if ((c = vs_getChar(screen, x, j)) != ' ') {
	  flag = 1;
	  break;
	}
  }

  if (flag == 0) {
	/* safe walking down; try right */
	for (i = x; i < exit_x; i++) {
	  if ((c = vs_getChar(screen, i, exit_y)) != ' ') {
		flag = 1;
		break;
	  }
	}
	if (flag == 0) {
	  /* safe; make path down, then right */
	  vs_putChar(screen, 'v', x, y);
	  vs_putChar(screen, '>', x, exit_y);
	  set_vs_pos(curPosition, exit_x, exit_y, RIGHT);
	  return 0;
	}
  }

  /* can't go right or down without overwriting data */
  return -1;
}

/* call putChar with a vs_pos instead of x & y */
void vs_putCharPos(vs * screen, char c, vs_pos * curPosition) {
  vs_putChar(screen, c, curPosition -> pos_x, curPosition -> pos_y);

  /* update curPosition */
  /*
  if (curPosition -> dir == LEFT)
  (curPosition -> pos_x)--;
  if (curPosition -> dir == RIGHT)
  (curPosition -> pos_x)++;
  if (curPosition -> dir == UP)
  (curPosition -> pos_y)--;
  if (curPosition -> dir == DOWN)
  (curPosition -> pos_y)++;
  */
}
