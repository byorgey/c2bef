/* beflib.c

   Various befunge-specific code (i.e. Code You Would Never Ever Need
   Anywhere Else)

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#include "beflib.h"

#include <math.h>

int befreplen ( long int n ) {
  int div, a;

  if ( n < 0 )
    return 2 + befreplen ( -n );
  else if ( n < 16 )
    return 1;
  else if ( n < 31 )
    return 3;
  else {
    if ( n <= 225 ) {
      for ( div = 2; div < 16; div++ ) {
	if ( n / div * div == n && n / div < 16 ) {
	  return 1 + befreplen(div) + befreplen(n/div);
	}
      }
    } 

    a = (int)(sqrt((double)(n)));
    if ( a*a == n )
      return befreplen(a) + 2;
    else 
      return befreplen(a) + 2 + befreplen(n - a*a) + 1;
  }
}

/* Takes a string, a position in the string, and a number.  Generate
   the Befunge representation for n starting at pos, and return the
   next empty index. */

int num2befreppos ( char * str, int pos, long int n ) {
  int nextindex, div, a;

  if ( n < 0 ) {

    str[pos] = '0';
    nextindex = num2befreppos ( str, 1, -n );
    str[nextindex] = '-';
    return nextindex + 1;

  } else if ( n < 10 ) {
    
    str[pos] = (char)(n + '0');
    return pos + 1;

  } else if ( n < 16 ) {

    str[pos] = (char)(n - 10 + 'a');
    return pos + 1;

  } else if ( n < 31 ) {

    num2befreppos ( str, pos, 15 );
    num2befreppos ( str, pos + 1, n - 15 );
    str[pos + 2] = '+';

    return pos + 3;

  } else {

    if ( n <= 225 ) {
      for ( div = 2; div < 16; div++ ) {
	if ( n / div * div == n && n / div < 16 ) {
	  nextindex = num2befreppos(str, pos, div);
	  nextindex = num2befreppos(str, nextindex, n/div);
	  str[nextindex] = '*';
	  return nextindex + 1;
	}
      }
    }
      
    a = (int)(sqrt((double)(n)));
    if ( a*a == n ) {
      nextindex = num2befreppos(str, pos, a);
      str[nextindex] = ':';
      str[nextindex + 1] = '*';
      return nextindex + 2;
    } else {
      nextindex = num2befreppos(str, pos, a);
      str[nextindex] = ':';
      str[nextindex + 1] = '*';
      nextindex = num2befreppos(str, nextindex + 2, n - a*a);
      str[nextindex] = '+';
      return nextindex + 1;
    }
  }
}

char * num2befrep ( long int num ) {

  int len = befreplen ( num );

  char * str = (char *) malloc ( (len + 1) * sizeof(char) );
  str[len] = '\0';

  num2befreppos ( str, 0, num );
 
  return str;
}
