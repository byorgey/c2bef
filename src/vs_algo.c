/* vs_algo.c

   Implementation of various yummy algorithms for doing things with
   vs-gloms.

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@
*/

#include "vs_algo.h"

void unvisit ( vs * s ) {
  if ( s == NULL ) return;

  if ( s->visited ) {
    s->visited = 0;
    unvisit ( s->leftscreen );
    unvisit ( s->rightscreen );
    unvisit ( s->upscreen );
    unvisit ( s->downscreen );
  }
}

int max ( int a, int b ) {
  return (a > b) ? a : b;
}

void offer_maxright ( vs * screen, int offered_value )
{
  if ( screen == NULL ) return;

  /* If the offered value is better than the one we currently have,
     take it and tell all our friends! 
  */
  if ( offered_value > screen->maxright ) {
    screen->maxright = offered_value;

    /* We found a better value -- tell all our friends! */
    offer_maxright ( screen->rightscreen, screen->maxright - 1 );
    offer_maxright ( screen->upscreen, screen->maxright );
    offer_maxright ( screen->leftscreen, screen->maxright + 1);
    offer_maxright ( screen->downscreen, screen->maxright );
  }
}

int calc_max_right_r ( vs * screen ) {

  int max_rec_val;

  /* Base case. */
  if ( screen == NULL ) 
    return 0;

  else if ( screen->visited ) {

    /* We've been here before - just return the current best value. */
    return screen->maxright;

  } else {

    /* We've been visited. */
    screen->visited = 1;

    /* Calculate our value recursively. */
    max_rec_val = 1 + calc_max_right_r ( screen->rightscreen );
    max_rec_val = max ( max_rec_val, calc_max_right_r ( screen->upscreen ) );
    max_rec_val = max ( max_rec_val, calc_max_right_r ( screen->leftscreen ) - 1 );
    max_rec_val = max ( max_rec_val, calc_max_right_r ( screen->downscreen ) );

    offer_maxright ( screen, max_rec_val );

    return screen->maxright;
  }
}

void calc_max_right ( vs * screen ) {
  calc_max_right_r ( screen );
  unvisit ( screen );
}


void offer_maxdown ( vs * screen, int offered_value )
{
  if ( screen == NULL ) return;

  /* If the offered value is better than the one we currently have,
     take it and tell all our friends! 
  */
  if ( offered_value > screen->maxdown ) {
    screen->maxdown = offered_value;

    /* We found a better value -- tell all our friends! */
    offer_maxdown ( screen->downscreen, screen->maxdown - 1 );
    offer_maxdown ( screen->rightscreen, screen->maxdown );
    offer_maxdown ( screen->upscreen, screen->maxdown + 1);
    offer_maxdown ( screen->leftscreen, screen->maxdown );
  }
}

int calc_max_down_r ( vs * screen ) {

  int max_rec_val;

  /* Base case. */
  if ( screen == NULL ) 
    return 0;

  else if ( screen->visited ) {

    /* We've been here before - just return the current best value. */
    return screen->maxdown;

  } else {

    /* We've been visited. */
    screen->visited = 1;

    /* Calculate our value recursively. */
    max_rec_val = 1 + calc_max_down_r ( screen->downscreen );
    max_rec_val = max ( max_rec_val, calc_max_down_r ( screen->rightscreen ) );
    max_rec_val = max ( max_rec_val, calc_max_down_r ( screen->upscreen ) - 1 );
    max_rec_val = max ( max_rec_val, calc_max_down_r ( screen->leftscreen ) );

    offer_maxdown ( screen, max_rec_val );

    return screen->maxdown;
  }
}

void calc_max_down ( vs * screen ) {
  calc_max_down_r ( screen );
  unvisit ( screen );
}

/* We just have to find the global max of the maxrights. */
int calc_width_r ( vs * screen ) {
  int width;

  if ( screen == NULL || screen->visited )
    return 0;
  else {
    screen->visited = 1;

    width = max ( screen->maxright, calc_width_r ( screen->rightscreen ) );
    width = max ( width, calc_width_r ( screen->upscreen ) );
    width = max ( width, calc_width_r ( screen->leftscreen ) );
    width = max ( width, calc_width_r ( screen->downscreen ) );

    return width;
  }
}

int calc_width ( vs * screen ) {
  int w = calc_width_r ( screen );
  unvisit ( screen );
  return w;
}


/* Find the global max of the maxdowns. */
int calc_height_r ( vs * screen ) {
  int height;

  if ( screen == NULL || screen->visited )
    return 0;
  else {
    screen->visited = 1;

    height = max ( screen->maxdown, calc_height_r ( screen->rightscreen ) );
    height = max ( height, calc_height_r ( screen->upscreen ) );
    height = max ( height, calc_height_r ( screen->leftscreen ) );
    height = max ( height, calc_height_r ( screen->downscreen ) );

    return height;
  }
}

int calc_height ( vs * screen ) {
  int h = calc_height_r ( screen );
  unvisit ( screen );
  return h;
}


vs *** make_table ( int w, int h ) 
{
  int i, j;

  vs *** table = (vs ***) malloc ( sizeof(vs **) * w );
  for ( i = 0; i < w; i++ ) {

    table[i] = (vs **) malloc ( sizeof(vs *) * h );
    for ( j = 0; j < h; j++ ) 
      table[i][j] = NULL;

  }

  return table;
}

void destroy_table ( vs *** table, int w, int h )
{
  int i;
  for ( i = 0; i < w; i++ )
    free(table[i]);

  free(table);
}

void init_table_r ( vs *** table, int w, int h, vs * screen )
{
  if ( screen == NULL || screen->visited )
    return;
  else {
    screen->visited = 1;

    if (table[w - screen->maxright][h - screen->maxdown] != NULL) {
      printf ( "BAD WEIRDNESS: trying to assign a second vs to table entry\n" );
      printf ( "with coordinates %d, %d\n", w - screen->maxright, h - screen->maxdown );
      exit(1);
    }

    table[w - screen->maxright][h - screen->maxdown] = screen;

    init_table_r ( table, w, h, screen->rightscreen );
    init_table_r ( table, w, h, screen->downscreen );
    init_table_r ( table, w, h, screen->leftscreen );
    init_table_r ( table, w, h, screen->upscreen );
  }
}

void init_table ( vs *** table, int w, int h, vs * screen )
{
  init_table_r ( table, w, h, screen );
  unvisit ( screen );
}

void fill_table ( vs *** table, int w, int h )
{
  int i, j;

  for ( i = 0; i < w; i++ ) {
    for ( j = 0; j < h; j++ ) {
      if ( table[i][j] == NULL ) {
	table[i][j] = vs_createEmpty ();
      }
    }
  }
}

void linkify ( vs *** table, int w, int h )
{
  int i, j;

  for ( i = 0; i < w; i++ ) {
    for ( j = 0; j < h; j++ ) {

      if ( i > 0 )
	table[i][j]->leftscreen = table[i-1][j];
      if ( i < (w-1) )
	table[i][j]->rightscreen = table[i+1][j];
      if ( j > 0 )
	table[i][j]->upscreen = table[i][j-1];
      if ( j < (h-1) )
	table[i][j]->downscreen = table[i][j+1];

      /* Yes, this really should be (i+1). The reason is that the
	 first time fill_blanks() is called, the constant flinger has
	 not been yet added to the lefthand side (it cannot be,
	 because it needs to know the global position of each vs,
	 which cannot be known until everything is linkified... ) 
      */
      table[i][j]->global_x = (i + 1) * VS_WIDTH;  
      table[i][j]->global_y = j * VS_HEIGHT;

    }
  }
}

void fill_blanks ( vs * screen ) 
{
  int w, h;
  vs *** table;

  /* Calculate the size of a bounding rectangle */
  calc_max_right ( screen );
  calc_max_down ( screen );

  w = calc_width ( screen );
  h = calc_height ( screen );

  /* Make a sufficiently large table and fill it in with the proper
     vs's */
  table = make_table ( w, h );
  init_table ( table, w, h, screen );

  /* Now fill any NULL spots in the table with special empty vs's. */
  fill_table ( table, w, h );

  /* Now make sure everything is linked up properly. (Also set global
     x and y's) */
  linkify ( table, w, h );
  
  /* Now delete the table. */
  destroy_table ( table, w, h );
}

void glom_build_list_r ( DLL list, vs * screen )
{
  if ( screen == NULL ) return;

  if ( !screen->visited ) {
    screen->visited = true;

    DLL_InsertBack ( list, screen );

    glom_build_list_r ( list, screen->rightscreen );
    glom_build_list_r ( list, screen->upscreen );
    glom_build_list_r ( list, screen->leftscreen );
    glom_build_list_r ( list, screen->downscreen );
  }
}

void destroy_glom ( vs * screen )
{
  DLL list = DLL_Create();
  vs * cur;

  glom_build_list_r ( list, screen );

  for ( DLL_IterBegin ( list ); !DLL_IterAtEnd ( list ); DLL_IterNext ( list ) ) {
    cur = (vs *) DLL_IterCurrent ( list );
    vs_destroy(cur);
  }

  DLL_MakeEmpty ( list );
  DLL_Destroy ( list );
}

vs * glomify ( DLL vs_list )
{
  vs * head, * cur_vs, * prev_vs = NULL;

  head = DLL_Front ( vs_list );

  for ( DLL_IterBegin(vs_list); !DLL_IterAtEnd(vs_list); DLL_IterNext(vs_list) ) {
    cur_vs = (vs *) DLL_IterCurrent(vs_list);

    if ( prev_vs != NULL )
      vs_setVS ( prev_vs, cur_vs, RIGHT );

    while ( cur_vs->rightscreen != NULL ) cur_vs = cur_vs->rightscreen;

    prev_vs = cur_vs;
  }

  return head;
}
