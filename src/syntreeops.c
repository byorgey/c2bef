/* syntreeops.c

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0a"3002 )c( thgirypoc">:#,_$@

*/

#include "syntreeops.h"

/* Initial set-up of the syntax tree */
void setup_syntaxtree ( node * treeroot ) {

  /* Do nothing for now ??? */
}

/* Delete the node passed.  And all of its children too!  Mwahahaha!!! */
void deletetree(node * treeroot) {
  if (treeroot != NULL) {
	if (treeroot -> unk.type != Nident &&
		treeroot -> unk.type != Nconst &&
		treeroot -> unk.type != Nrefvar) {
	  /* delete node's children first */
	  deletetree(treeroot -> internal.child[0]);
	  deletetree(treeroot -> internal.child[1]);
	  deletetree(treeroot -> internal.child[2]);
	}
	free(treeroot);
  }
}

/* makelist - Add 'newnode' to the (possibly empty) list 'list'.  */
struct listhead makelist( struct listhead list, node *newnode, int line) {

  if ( list.head == NULL ) {
    list.head = list.tail = newnode;
  } else {
    list.tail->internal.child[1] = newnode;
    list.tail = newnode;
  }

  return list;
}

/* makesingle  -  Make a node with a one child.    */
node * makesingle(nodetype op, node *child, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> internal.type = op;
  newNode -> internal.line = line;
  newNode -> internal.child[0] = child;
  newNode -> internal.child[1] = NULL;
  newNode -> internal.child[2] = NULL;

  return newNode;
}	

/* makedouble  -  Make a node with a two children.    */
node * makedouble(nodetype op, node *child1, node *child2, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> internal.type = op;
  newNode -> internal.line = line;
  newNode -> internal.child[0] = child1;
  newNode -> internal.child[1] = child2;
  newNode -> internal.child[2] = NULL;
  
  return newNode;
}	

/* maketriple  -  Make a node with a three children.    */
node * maketriple(nodetype op, node *child1, node *child2, node *child3, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> internal.type = op;
  newNode -> internal.line = line;
  newNode -> internal.child[0] = child1;
  newNode -> internal.child[1] = child2;
  newNode -> internal.child[2] = child3;
  
  return newNode;
}	

/* Create a node representing a memory reference.         */
/* Not doing nifty displacement thingy */
node * makerefvar(node * baseaddr, dcldesc * var, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> var.type = Nrefvar;
  newNode -> var.line = line;
  newNode -> var.baseaddr = baseaddr;
  newNode -> var.vardesc = var;

  return newNode;
}

/* Create a tree node for a constant value.  For now, a new node is created */
/* for each constant.  Eventually, this may have to be changed for value    */
/* numbering such that a single copy of each consant is created.            */
node * makeconst(int value, int ischar, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> constant.type = Nconst;
  newNode -> constant.line = line;
  newNode -> constant.value = value;
  newNode -> constant.ischar = ischar;

  return newNode;
}

/* Create a tree node for an identifier. */
node * makeident(identdesc * varptr, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> ident.type = Nident;
  newNode -> ident.line = line;
  newNode -> ident.ident = varptr;
  newNode -> ident.decl = NULL;

  return newNode;
}

/* Create a tree node for a string literal. */
node * makestring(char * string, int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> string.type = Nstring;
  newNode -> string.line = line;
  newNode -> string.strvalue = string;

  return newNode;
}

/* errornode   -  Return an Nerror node */
node * errornode(int line) {
  node * newNode = (node *)malloc(sizeof(node));
  newNode -> unk.type = Nerror;
  newNode -> unk.line = line;

  return newNode;
}

void printlineinfo ( node * treeroot ) {
  if ( treeroot != NULL ) {
    printf(" (%d) ", treeroot -> unk.type);
    printf("Line %d\n",treeroot -> unk.line);
  }
}

/* print out syntax tree */
void printsyntree(node * treeroot, int indent) {
  int i, child, numchildren;

  for (i = 0; i < indent; i++) printf(" ");

  if ( treeroot == NULL ) {
    printf("NULL\n");
  } else {
	
    switch (treeroot -> unk.type) {
    case Nident:
      printf("IDENT");
      printlineinfo ( treeroot );
      printidentdesc(treeroot -> ident.ident, indent + 2);
      printdcldesc(treeroot -> ident.decl, indent + 2);
      break;
    case Nconst:
      if (treeroot -> constant.ischar)
	printf("CONST: value = %d, ischar = YES (char value = %c)",
	       treeroot -> constant.value, treeroot -> constant.value);
      else
	printf("CONST: value = %d, ischar = NO", treeroot -> constant.value);
      printlineinfo ( treeroot );
      break;
    case Nrefvar:
      printf("REFVAR");
      printlineinfo ( treeroot );
      printsyntree(treeroot -> var.baseaddr,indent + 2);
      printdcldesc(treeroot -> var.vardesc, indent + 2);
      break;
	case Nstring:
	  printf("STRING");
	  printlineinfo ( treeroot );
	  printf("%s\n",treeroot -> string.strvalue);
	  break;
    default:
	  printnameoftype(treeroot -> unk.type);
      printlineinfo(treeroot);
	  numchildren = getnumberofchildren(treeroot -> unk.type);
	  for (child = 0; child < numchildren; child++) {
		printsyntree(treeroot -> internal.child[child], indent + 2);
	  }
      break;
    }
  }
}

/* returns number of children for the given node type */
int getnumberofchildren(nodetype type) {
  switch (type) {

  case Npointer:
  case Nreturn:
  case Nnot:
  case Nneg:
  case Npos:
  case Nbitneg:
  case Nderef:
  case Nincpre:
  case Ndecpre:
  case Nincpost:
  case Ndecpost:
  case Nsizeof:
	return 1;

  case Ndefnlist:
  case Narray:
  case Nfuncsig:
  case Nstruct:
  case Nfieldlist:
  case Ndecl:
  case Ninitdecl:
  case Ninitdecllist:
  case Ninitlist:
  case Ndecllist:
  case Ncompound:
  case Nformallist:
  case Nactuallist:
  case Nparam:
  case Nstmtlist:
  case Ncall:
  case Nwhile:
  case Nselect:
  case Nsubs:
  case Ngets:
  case Ntimesgets:
  case Ndivgets:
  case Nplusgets:
  case Nminusgets:
  case Nmodgets:
  case Nshiftrgets:
  case Nshiftlgets:
  case Nandgets:
  case Norgets:
  case Nxorgets:
  case Nor:
  case Nand:
  case Nbitor:
  case Nbitand:
  case Nbitxor:
  case Nshiftleft:
  case Nshiftright:
  case Nlt:
  case Ngt:
  case Neq:
  case Nle:
  case Nge:
  case Nne:
  case Nplus:
  case Nminus:
  case Ntimes:
  case Ndiv:
  case Nmod:
	return 2;
	
  case Nfuncdefn:
  case Nif:
  case Nstupidop:
	return 3;

  case Nerror:
  case Nident:
  case Nconst:
  case Nrefvar:
  case Nstring:
  case Ncontinue:
  case Nbreak:
  default:
	return 0;
  }
}

/* prints the name of the given node type. */
void printnameoftype(nodetype type) {
  switch (type) {
  case Nerror:
	printf("ERROR\n");
	break;
  case Nident:
	printf("IDENT");
	break;
  case Nconst:
	printf("CONST");
	break;
  case Nrefvar:
	printf("REFVAR");
	break;
  case Nstring:
	printf("STRING");
	break;
  case Ndefnlist:
	printf("DEFNLIST");
	break;
  case Narray:
	printf("ARRAY");
	break;
  case Npointer:
	printf("*" );
	break;
  case Nfuncsig:
	printf("FUNCSIG" );
	break;
  case Nstruct:
	printf("STRUCT");
	break;
  case Nfieldlist:
	printf("FIELDLIST");
	break;
  case Ndecl:
	printf("DECL");
	break;
  case Ninitdecl:
	printf("INITDECL");
	break;
  case Ninitdecllist:
	printf("INITDECLLIST");
	break;
  case Ninitlist:
	printf("INITLIST" );
	break;
  case Ndecllist:
	printf("DECLLIST" );
	break;
  case Nfuncdefn:
	printf("FUNCDEFN");
	break;
  case Ncompound:
	printf("COMPOUND");
	break;
  case Nformallist:
	printf("FORMALLIST");
	break;
  case Nactuallist:
	printf("ACTUALLIST");
	break;
  case Nparam:
	printf("PARAM");
	break;
  case Nstmtlist:
	printf("STMTLIST");
	break;
  case Ncall:
	printf("CALL");
	break;
  case Nreturn:
	printf("RETURN");
	break;
  case Nif:
	printf("IF");
	break;
  case Nwhile:
	printf("WHILE");
	break;
  case Ncontinue:
	printf("CONTINUE");
	break;
  case Nbreak:
	printf("BREAK");
	break;
  case Nselect:
	printf("SELECT");
	break;
  case Nsubs:
	printf("SUBS");
	break;
  case Ngets:
	printf("GETS");
	break;
  case Ntimesgets:
	printf("TIMESGETS");
	break;
  case Ndivgets:
	printf("DIVGETS");
	break;
  case Nplusgets:
	printf("PLUSGETS");
	break;
  case Nminusgets:
	printf("MINUSGETS");
	break;
  case Nmodgets:
	printf("MODGETS");
	break;
  case Nshiftrgets:
	printf("SHIFTRGETS");
	break;
  case Nshiftlgets:
	printf("SHIFTLGETS");
	break;
  case Nandgets:
	printf("ANDGETS");
	break;
  case Norgets:
	printf("ORGETS");
	break;
  case Nxorgets:
	printf("XORGETS");
	break;
  case Nnot:
	printf("NOT");
	break;
  case Nneg:
	printf("NEG");
	break;
  case Npos:
	printf("POS");
	break;
  case Nbitneg:
	printf("BITNEG");
	break;
  case Nderef:
	printf("DEREF");
	break;
  case Nincpre:
	printf("INCPRE");
	break;
  case Ndecpre:
	printf("DECPRE");
	break;
  case Nincpost:
	printf("INCPOST");
	break;
  case Ndecpost:
	printf("DECPOST");
	break;
  case Nsizeof:
	printf("SIZEOF");
	break;
  case Nor:
	printf("OR");
	break;
  case Nand:
	printf("AND");
	break;
  case Nbitor:
	printf("BITOR");
	break;
  case Nbitand:
	printf("BITAND");
	break;
  case Nbitxor:
	printf("BITXOR");
	break;
  case Nshiftleft:
	printf("SHIFTLEFT");
	break;
  case Nshiftright:
	printf("SHIFTRIGHT");
	break;
  case Nlt:
	printf("LT (<)");
	break;
  case Ngt:
	printf("GT (>)");
	break;
  case Neq:
	printf("EQ (=)");
	break;
  case Nle:
	printf("LE (<=)");
	break;
  case Nge:
	printf("GE (>=)");
	break;
  case Nne:
	printf("NE (!=)");
	break;
  case Nplus:
	printf("PLUS");
	break;
  case Nminus:
	printf("MINUS");
	break;
  case Ntimes:
	printf("TIMES");
	break;
  case Ndiv:
	printf("DIV");
	break;
  case Nmod:
	printf("MOD");
	break;
  case Nstupidop:
	printf("STUPIDOP (?:)");
	break;
  default:
	printf("ERROR: default case reached");
	break;
  }
}

/* evaluates a node's subtree's constant expressions, prunes them, and creates a new Nconst node */
void foldconstant (node * treeroot) {
  long i, j;
  if (treeroot == NULL || treeroot -> unk.type == Nconst) return;

  i = determinevalue(treeroot);
  
  /* at this point, if treeroot is anything but a valid internalnode, determinevalue() would have
	 ended the program.  so we can assume that it is an internalnode and keep going. */
  for (j = 0; j < 3; j++) deletetree(treeroot -> internal.child[j]);

  /* now, turn treeroot into an Nconst */
  treeroot -> unk.type = Nconst;
  treeroot -> constant.value = i;
  /* we'll assume that the constant isn't a char, because if it was,
	 why is this function being called anyway? */
  treeroot -> constant.ischar = 0;
}

/* returns the value of a node and its subtrees */
long determinevalue (node * treeroot) {
  if (treeroot == NULL) {
	printf("BAD WEIRDNESS: This should not be NULL.  (Line %d)\n",treeroot -> unk.line);
	exit(REAL_FAST);
  }

  switch (treeroot -> unk.type) {
  case Nconst:
	return (treeroot -> constant.value);

  case Nnot:
	return (!determinevalue(treeroot -> internal.child[0]));
  case Nneg:
	return (-determinevalue(treeroot -> internal.child[0]));
  case Npos:
	return (determinevalue(treeroot -> internal.child[0]));
  case Nbitneg:
	return (~determinevalue(treeroot -> internal.child[0]));
  case Nor:
	return (determinevalue(treeroot -> internal.child[0]) ||
			determinevalue(treeroot -> internal.child[1]));
  case Nand:
	return (determinevalue(treeroot -> internal.child[0]) &&
			determinevalue(treeroot -> internal.child[1]));
  case Nbitor:
	return (determinevalue(treeroot -> internal.child[0]) |
			determinevalue(treeroot -> internal.child[1]));
  case Nbitand:
	return (determinevalue(treeroot -> internal.child[0]) &
			determinevalue(treeroot -> internal.child[1]));
  case Nbitxor:
	return (determinevalue(treeroot -> internal.child[0]) ^
			determinevalue(treeroot -> internal.child[1]));
  case Nshiftleft:
	return (determinevalue(treeroot -> internal.child[0]) <<
			determinevalue(treeroot -> internal.child[1]));
  case Nshiftright:
	return (determinevalue(treeroot -> internal.child[0]) >>
			determinevalue(treeroot -> internal.child[1]));
  case Nlt:
	return (determinevalue(treeroot -> internal.child[0]) <
			determinevalue(treeroot -> internal.child[1]));
  case Ngt:
	return (determinevalue(treeroot -> internal.child[0]) >
			determinevalue(treeroot -> internal.child[1]));
  case Neq:
	return (determinevalue(treeroot -> internal.child[0]) ==
			determinevalue(treeroot -> internal.child[1]));
  case Nle:
	return (determinevalue(treeroot -> internal.child[0]) <=
			determinevalue(treeroot -> internal.child[1]));
  case Nge:
	return (determinevalue(treeroot -> internal.child[0]) >=
			determinevalue(treeroot -> internal.child[1]));
  case Nne:
	return (determinevalue(treeroot -> internal.child[0]) !=
			determinevalue(treeroot -> internal.child[1]));
  case Nplus:
	return (determinevalue(treeroot -> internal.child[0]) +
			determinevalue(treeroot -> internal.child[1]));
  case Nminus:
	return (determinevalue(treeroot -> internal.child[0]) -
			determinevalue(treeroot -> internal.child[1]));
  case Ntimes:
	return (determinevalue(treeroot -> internal.child[0]) *
			determinevalue(treeroot -> internal.child[1]));
  case Ndiv:
	return (determinevalue(treeroot -> internal.child[0]) /
			determinevalue(treeroot -> internal.child[1]));
  case Nmod:
	return (determinevalue(treeroot -> internal.child[0]) %
			determinevalue(treeroot -> internal.child[1]));
  case Nstupidop:
	return (determinevalue(treeroot -> internal.child[0]) ?
			determinevalue(treeroot -> internal.child[1]) :
			determinevalue(treeroot -> internal.child[2]));
  case Ngets:
  case Ntimesgets:
  case Ndivgets:
  case Nplusgets:
  case Nminusgets:
  case Nmodgets:
  case Nshiftrgets:
  case Nshiftlgets:
  case Nandgets:
  case Norgets:
  case Nxorgets:
	/* return the value of the right-hand side of the expression */
	return (determinevalue(treeroot -> internal.child[1]));
  default:
	printf("BAD BAD:  Invalid node type in constant evaluation: ");
	printnameoftype(treeroot -> unk.type);
	printf(" at line %d\n",treeroot -> unk.line);
	exit(REAL_FAST);
  }
}

/* finds the next non-refvar node */
node * findnonrefvar (node * treeroot) {
  if (treeroot == NULL) return NULL;
  if (treeroot -> unk.type == Nrefvar)
	return findnonrefvar(treeroot -> var.baseaddr);
  return treeroot;
}

/* reduces all refvar's to one only */
void collapsetree (node * treeroot) {
  node * tempNode;
  node * iterator, * iterator2;
  if (treeroot == NULL) return;
  switch (treeroot -> unk.type) {
  case Nrefvar:
	tempNode = findnonrefvar (treeroot -> var.baseaddr);
	iterator = iterator2 = (treeroot -> var.baseaddr);
	while (iterator != tempNode) {
	  iterator2 = iterator -> var.baseaddr;
	  free(iterator);
	  iterator = iterator2;
	}
	treeroot -> var.baseaddr = tempNode;
	collapsetree(treeroot -> var.baseaddr);
	return;
  case Nconst:
  case Nident:
  case Nstring:
	return;
  default:
	collapsetree(treeroot -> internal.child[0]);
	collapsetree(treeroot -> internal.child[1]);
	collapsetree(treeroot -> internal.child[2]);
	return;
  }
}
