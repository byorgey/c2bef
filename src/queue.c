/* queue.h

   Queue ADT, based on doubly-linked list implementation in dll.{h,c}

   C-to-Befunge translation project

   (c) 2003 Steve Winslow and Brent Yorgey 
*/

#include "queue.h"


Queue Queue_Create () {
  Queue s = DLL_Create();

  return s;
}


status_code Queue_Clear ( Queue queue ) {
  return DLL_Clear(queue);
}


status_code Queue_Destroy ( Queue queue ) {
  return DLL_Destroy(queue);
}


bool Queue_IsEmpty ( const Queue queue ) {
  return DLL_IsEmpty ( queue );
}

long int Queue_Size ( const Queue queue ) {
  return DLL_Size ( queue );
}


Item_ptr Queue_Front ( const Queue queue ) {
  return DLL_Front ( queue );
}

status_code Queue_Enqueue ( Queue queue, Item_ptr item ) {
  return DLL_InsertBack ( queue, item );
}

status_code Queue_Dequeue ( Queue queue ) {
  return DLL_DeleteFront ( queue );
}


void Queue_Traverse ( Queue queue, void (*ProcessItem) (Item_ptr item) ) {
  DLL_Traverse ( queue, ProcessItem );
}
  
