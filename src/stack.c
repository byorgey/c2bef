/* stack.h

   Stack ADT, based on doubly-linked list implementation in dll.{h,c}

   C-to-Befunge translation project

   (c) 2003 Steve Winslow and Brent Yorgey 
*/

#include "stack.h"


Stack Stack_Create () {
  Stack s = DLL_Create();

  return s;
}


status_code Stack_Clear ( Stack stack ) {
  return DLL_Clear(stack);
}


status_code Stack_Destroy ( Stack stack ) {
  return DLL_Destroy(stack);
}


bool Stack_IsEmpty ( const Stack stack ) {
  return DLL_IsEmpty ( stack );
}

long int Stack_Size ( const Stack stack ) {
  return DLL_Size ( stack );
}


Item_ptr Stack_Top ( const Stack stack ) {
  return DLL_Back ( stack );
}

status_code Stack_Push ( Stack stack, Item_ptr item ) {
  return DLL_InsertBack ( stack, item );
}

status_code Stack_Pop ( Stack stack ) {
  return DLL_DeleteBack ( stack );
}


void Stack_Traverse ( Stack stack, void (*ProcessItem) (Item_ptr item) ) {
  DLL_TraverseBackwards ( stack, ProcessItem );
}
  
