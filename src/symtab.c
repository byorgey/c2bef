/* symtab.c

   C to Befunge translator

   Steve Winslow and Brent Yorgey
   Winter Study 2003

   >0av"3002 )c( thgirypoc">:#,_$@
*/

#include <stdio.h>
#include "symtab.h"

#include "syntreeops.h"

#include "befgen.h"

/* THE symbol table. */
extern DLL symboltable;

/* THE syntax tree. */
extern node * syntaxtree;

/* "State" variables to use while traversing the syntax tree. */
Stack cur_base_type;  /* Stack of base types in effect. */
dcldesc * cur_declarator;  /* Current declarator to which we should
			      link the next declarator we see. */
DLL local_vars;   /* List of local variables encountered in the
		     current function definition. */
node * cur_code_root;  /* Root node for code of current function. */

bool local_scope;

int local_offset;
int global_offset = 0;

identdesc *int_node;
identdesc *char_node;
identdesc *long_node;
identdesc *void_node;


void printdcldesc_wrapper ( void * item ) {
  dcldesc * dcl = (dcldesc *)item;

  if ( dcl->unk.root_entry ) { 
    printdcldesc ( dcl, 0 );
  } 
}

/* printdcldesc --  Print out the information found in a declaration  */
/*                  descriptor.                                       */
void printdcldesc(dcldesc * desc, int indent) {

  int i;
  dcldesc * scan;

  for (i = 0; i < indent; i++) printf(" ");

  printf ( "%x ", desc );

  if ( desc == NULL ) {
    printf("NULL\n");
  } else {

    switch (desc -> unk.type) {

    case typedecl:
      switch ( desc -> type.typetype ) {
      case INT_T: printf ( "int\n" ); break;
      case CHAR_T: printf ( "char\n" ); break;
      case LONG_T: printf ( "long\n" ); break;
      case VOID_T: printf ( "void\n" ); break;
      }
      break;

    case funcdecl:
      printf("(funcdesc), line %d\n",
	     desc -> func.line);

      printdcldesc ( desc-> func.rettype, indent + 2 );

      scan = desc -> func.formallist;
      while ( scan != NULL ) {
	printdcldesc ( scan, indent + 2 );
	scan = scan -> formal.formallink;
      }

      printsyntree ( desc -> func.coderoot, indent + 2 );

      break;

    case vardecl:

      if ( desc -> var.ident != NULL ) {
	printf("%s (vardesc), line %d\n",
	       desc -> var.ident -> name,
	       desc -> var.line);
      } else {

	printf("(anon vardesc), line %d\n", desc -> var.line );
	}
      printdcldesc(desc -> var.vartype, indent + 2);
      break;

    case formaldecl:
      printf("(formal)\n" );
      printdcldesc(desc -> formal.vartype, indent );
      break;

    case arraydecl:
      printf("(arraydecl) size %d\n", desc -> array.size);
      printdcldesc(desc -> array.vartype, indent + 2);
      break;

    case structdecl:
      printf("(struct), line %d\n", desc -> structure.line);
      scan = desc -> structure.fields;

      while ( scan != NULL ) {
	printdcldesc(scan -> field.vartype, indent + 2);
	scan = scan -> field.fieldlink;
      }
      break;

    case fielddecl:
      /* Handled in the case for structdecl */
      break;

    case pointerdecl:
      printf("*\n");
      printdcldesc(desc -> pointer.vartype, indent + 2);
      break;

    default:
      printf("unrecognized\n");
    }
  }
}

/* ??? Update this to print global & local pointers. */
void printidentdesc ( identdesc * id, int indent )
{
  int i;
  for ( i = 0; i < indent; i++ ) printf(" ");

  printf("%s\n", id->name );
}

/* lookup --- Given a string, this routine returns the identifier      */
/*            descriptor for that string (creating one if necessary.   */
identdesc *lookup(char * name) {
  
  identdesc * id_entry = Hash_Lookup ( idHash, name );
  if ( id_entry == NULL ) {

    /* The identifier isn't already in the hash - create a new entry */
    id_entry = (identdesc *) malloc (sizeof(identdesc));
    
    id_entry->name = (char *) malloc (sizeof(char) * strlen(name) + 1);
    
    strcpy ( id_entry->name, name );
    id_entry->globaldecl = id_entry->localdecl = NULL;

    Hash_Insert ( idHash, id_entry, name );
  }

  return id_entry;
}

identdesc * lookup_wo_insert(char * name) { 
  return (identdesc *) Hash_Lookup ( idHash, name );
}
  

/* Hash-specific functions */
int stringHash ( void * str_ptr )
{
  char * str = (char *)str_ptr;

  int i = 0;
  int accum = 0;

  while ( str[i] != '\0' ) {
    accum += (int)str[i++];
  }

  return accum;
}


bool identdescMatches ( void * str_ptr, void * iddesc_ptr )
{
  char * str = (char *)str_ptr;
  identdesc * id = (identdesc *)iddesc_ptr;

  return (strcmp(id->name, str) == 0);
}

void printidentdesc_hash ( void * identdesc_ptr )
{
  identdesc * id = (identdesc *)identdesc_ptr;

  printidentdesc(id, 0);
}


/**********************************************************************/

void insert_special_hash_entries ()
{
  int_node = lookup("int");
  char_node = lookup("char");
  long_node = lookup("long");
  void_node = lookup("void");
}

dcldesc * insert_dcldesc ( decltype type, identdesc * id, int lineno ) {
  dcldesc * dcl = (dcldesc *) malloc ( sizeof (dcldesc) );

  dcl->unk.type = type;
  dcl->unk.ident = id;
  dcl->unk.line = lineno;
  dcl->unk.root_entry = true; /* assume this is a root entry until
				 proven otherwise */

  DLL_InsertBack ( symboltable, dcl );

  return dcl;
}

void set_base_type ( node * root ) {
  Stack_Push ( cur_base_type, process_declarator ( root ) );
  cur_declarator = Stack_Top ( cur_base_type );
}

void restore_base_type () {
  Stack_Pop ( cur_base_type );
  cur_declarator = Stack_Top ( cur_base_type );
} 

int calc_size ( dcldesc * entry )
{
  if ( entry == NULL ) return 0;
 
  switch ( entry->unk.type ) {
  case vardecl: 
    return calc_size ( entry->var.vartype );

  case pointerdecl:
    return 2;

  case arraydecl:
    return entry->array.size * calc_size ( entry->array.vartype );

  default:
    return 1;
  }
}

dcldesc * process_formallist ( node * root )
{
  node * param;
  dcldesc * dcl;

  if ( root == NULL ) return NULL;

  param = root->internal.child[0];

  if ( param->internal.child[0] == NULL ) {
    printf ( "What is this silly untyped parameter business?\n" );
    exit(1);
  }
  
  dcl = insert_dcldesc ( formaldecl, NULL, root->unk.line );

  set_base_type ( param->internal.child[0] );
  dcl->formal.vartype = process_declarator ( param->internal.child[1] );
  restore_base_type();

  dcl->formal.vartype->var.offset = local_offset;
  local_offset += calc_size ( dcl->formal.vartype );

  dcl->formal.formallink = process_formallist ( root->internal.child[1] );

  return dcl;
}

dcldesc * process_initdecl_fieldlist ( node * root )
{
  node * decl;
  dcldesc * dcl;
  
  if ( root == NULL ) return NULL;

  /* Reset current declarator */
  cur_declarator = Stack_Top ( cur_base_type );

  decl = root->internal.child[0]; /* Ninitdecl or declarator node */

  /* Make sure we have a declarator node (ignore initialization for now) */
  if ( decl->unk.type == Ninitdecl ) {
    decl = decl->internal.child[0];
  }

  dcl = insert_dcldesc ( fielddecl, NULL, decl->unk.line );
  dcl->field.vartype = process_declarator ( decl );
  dcl->field.fieldlink = process_initdecl_fieldlist ( root->internal.child[1] );

  return dcl;
}

dcldesc * process_fieldlist ( node * root )
{
  node * decllist, * initdecllist, * decl;
  dcldesc * dcl, * last_dcl;

  if ( root == NULL ) return NULL;
  else if ( root->unk.type != Nfieldlist ) {
    printf ( "non-Nfieldlist node passed to process_fieldlist\n" );
    exit(1);
  }

  decllist = root->internal.child[0];

  set_base_type ( decllist->internal.child[0] );
  
  initdecllist = decllist->internal.child[1];
  dcl = process_initdecl_fieldlist ( initdecllist );

  /* Link up the end of the list we just got to the next one */
  last_dcl = dcl;
  while ( last_dcl->field.fieldlink != NULL ) last_dcl = last_dcl->field.fieldlink;

  last_dcl->field.fieldlink = process_fieldlist ( root->internal.child[1] );

  restore_base_type();

  return dcl;
}

dcldesc * process_declarator ( node * root )
{
  dcldesc * dcl, * dcl2;

  char * name;
  identdesc * id;

  if ( root == NULL ) return NULL;

  switch ( root->unk.type ) {
    
  case Narray:
    dcl = insert_dcldesc ( arraydecl, NULL, root->unk.line );
    dcl->array.vartype = cur_declarator;

    /* Array size should be a constant. */
    if ( root->internal.child[1] == NULL )
      dcl->array.size = -1;
    else 
      dcl->array.size = determinevalue ( root->internal.child[1] );
    
    cur_declarator = dcl;
    dcl2 = process_declarator ( root->internal.child[0] );
      
    break;

  case Npointer:
    dcl = insert_dcldesc ( pointerdecl, NULL, root->unk.line );
    dcl->pointer.vartype = cur_declarator;

    cur_declarator = dcl;
    dcl2 = process_declarator ( root->internal.child[0] );

    break;

  case Nfuncsig:
    dcl = insert_dcldesc ( funcdecl, NULL, root->unk.line );

    dcl->func.rettype = cur_declarator;
    
    dcl->func.formallist = process_formallist ( root->internal.child[1] );

    dcl->func.coderoot = cur_code_root;

    cur_declarator = dcl;
    dcl2 = process_declarator ( root->internal.child[0] );

    break;
    
  case Nstruct:
    dcl = insert_dcldesc ( structdecl, NULL, root->unk.line );

    dcl->structure.fields = process_fieldlist ( root->internal.child[1] );

    cur_declarator = dcl;
    dcl2 = process_declarator ( root->internal.child[0] );

    break;

  case Nident:

    /* Special case if we see the name of a primitive type */
    if ( root->ident.ident->globaldecl == INT_TYPE ||
	 root->ident.ident->globaldecl == CHAR_TYPE ||
	 root->ident.ident->globaldecl == LONG_TYPE ||
	 root->ident.ident->globaldecl == VOID_TYPE    ) {

      dcl2 = root->ident.ident->globaldecl;

    } else { 

      dcl = insert_dcldesc ( vardecl, root->ident.ident, root->unk.line );

      /* If this variable is being declared as a previously named type (i.e. a struct),
	 we need to find the previous declaration of the struct. */
      if ( cur_declarator->unk.type == vardecl ) {

	name = cur_declarator->var.ident->name;

	if ( name == NULL ) {
	  printf ( "Error: Got empty string at location mongoose\n" );
	  exit(1);
	}

	id = lookup_wo_insert(name);
	if ( id == NULL ) {
	  printf ( "Error: %s not previously declared at line %d\n", name, root->unk.line );
	  exit(1);
	} 

	if ( id->localdecl == NULL ) {
	  dcl->var.vartype = id->globaldecl;
	  dcl->var.isGlobal = 1;
	} else {
	  dcl->var.vartype = id->localdecl;
	  dcl->var.isGlobal = 0;
	}
      } else {
	dcl->var.vartype = cur_declarator;
      }

      if ( !local_scope ) {
	if ( root->ident.ident->globaldecl != NULL ) {
	  printf ( "Error: Redeclaration of %s on line %d\n", root->ident.ident->name, root->unk.line);
	  exit(1);
	} else {
	  root->ident.ident->globaldecl = dcl;
	}

	dcl->var.offset = global_offset;
	global_offset += calc_size ( dcl->var.vartype );
      } else {
	DLL_InsertBack ( local_vars, root->ident.ident );

	if ( root->ident.ident->localdecl != NULL ) {
	  printf ( "Error: Redeclaration of %s on line %d\n", root->ident.ident->name, root->unk.line);
	  exit(1);
	} else {
	  root->ident.ident->localdecl = dcl;
	}

	dcl->var.offset = local_offset;
	local_offset += calc_size ( dcl->var.vartype );
      }	

      dcl2 = dcl;
    }
    
    break;

  default: 
    printf ( "Error: " );
    printnameoftype ( root->unk.type );
    printf ( " node encountered where declarator node type expected.\n" );
    exit(1);
    break;
  }

  return ( dcl2 == NULL ) ? dcl : dcl2;
}

void unlink_local_vars () {
  identdesc * var;

  for ( DLL_IterBegin ( local_vars ); !DLL_IterAtEnd ( local_vars ); DLL_IterNext ( local_vars ) ) {
    var = (identdesc *)DLL_IterCurrent ( local_vars );

    var->localdecl = NULL;
  }

  DLL_MakeEmpty ( local_vars );
}

void build_symtab_from_code ( node * root )
{
  if ( root == NULL ) return;

  switch ( root->unk.type ) {
  case Nident:
    if ( root->ident.ident->localdecl == NULL ) {
      root->ident.decl = root->ident.ident->globaldecl;
    } else {
      root->ident.decl = root->ident.ident->localdecl;
    }
    break;

  case Nrefvar:
    build_symtab_from_code ( root->var.baseaddr );
    break;

  case Nconst:
  case Nstring:
    break;

  default:
    build_symtab_from_code ( root->internal.child[0] );
    build_symtab_from_code ( root->internal.child[1] );
    build_symtab_from_code ( root->internal.child[2] );
    break;
  }
}   

void build_symtab_from_tree ( node * root ) 
{
  dcldesc * dcl;

  if ( root == NULL ) return;

  switch ( root->unk.type ) {

  case Ndefnlist:
    build_symtab_from_tree ( root->internal.child[0] );  /* Decl or funcdefn    */
    build_symtab_from_tree ( root->internal.child[1] );  /* More definitions... */
    break;

  case Ndecl:
    /* Keep track of the current base type */

    set_base_type ( root->internal.child[0] );

    /* Process the rest of the declaration */
    build_symtab_from_tree ( root->internal.child[1] );

    restore_base_type();
    break;
    
  case Ninitdecllist:
    build_symtab_from_tree ( root->internal.child[0] );  /* Item: Ninitdecl or declarator */

    cur_declarator = Stack_Top ( cur_base_type );        /* Reset base type */
    build_symtab_from_tree ( root->internal.child[1] );  /* Rest of the list */

    break;

  case Ninitdecl:
    build_symtab_from_tree ( root->internal.child[0] );  /* declarator node */

    /* Ignore initializers for now - generate code for them later. */
    break;
    
  case Narray:
  case Npointer:
  case Nfuncsig:
  case Nstruct:
  case Nident:

    process_declarator ( root );
    break;
    
    /* Function definitions *************************************/

  case Nfuncdefn:

    cur_code_root = root->internal.child[2];

    set_base_type ( root->internal.child[0] );       /* declarator */

    /* Reset the local_offset */
    local_offset = 0;

    process_declarator ( root->internal.child[1] );  /* Nfuncsig */

    restore_base_type();

    local_scope = true;
    build_symtab_from_tree ( root->internal.child[2] ); /* Ncompound */

    unlink_local_vars();
    local_scope = false;

    break;

  case Ncompound:

    build_symtab_from_tree ( root->internal.child[0] ); /* Ndecllist */
    build_symtab_from_code ( root->internal.child[1] ); /* Nstmtlist */
    break;

  case Ndecllist:
    
    build_symtab_from_tree ( root->internal.child[0] ); /* Ndecl */
    build_symtab_from_tree ( root->internal.child[1] ); /* etc. */
    break;
    
  default: return;
  }
}

void insert_special_symtab_entries () 
{
  INT_TYPE = insert_dcldesc ( typedecl, lookup("int"), 0 );
  CHAR_TYPE = insert_dcldesc ( typedecl, lookup("char"), 0 );
  LONG_TYPE = insert_dcldesc ( typedecl, lookup("long"), 0 );
  VOID_TYPE = insert_dcldesc ( typedecl, lookup("void"), 0 );

  INT_TYPE->type.typetype = INT_T;
  CHAR_TYPE->type.typetype = CHAR_T;
  LONG_TYPE->type.typetype = LONG_T;
  VOID_TYPE->type.typetype = VOID_T;
}
  
void mark_non_roots ( dcldesc * root, int level ) {
  if ( root == NULL ) return;

  if ( level != 0 ) root->unk.root_entry = false;

  switch ( root->unk.type ) {
    case funcdecl:
      mark_non_roots ( root->func.rettype, level + 1 );
      mark_non_roots ( root->func.formallist, level + 1 );
      break;

    case vardecl:
      mark_non_roots ( root->var.vartype, level + 1 );
      break;

    case fielddecl:
      mark_non_roots ( root->field.vartype, level + 1 );
      mark_non_roots ( root->field.fieldlink, level + 1 );
      break;

    case formaldecl:
      mark_non_roots ( root->formal.vartype, level + 1 );
      mark_non_roots ( root->formal.formallink, level + 1 );
      break;

    case arraydecl:
      mark_non_roots ( root->array.vartype, level + 1 );
      break;

    case structdecl:
      mark_non_roots ( root->structure.fields, level + 1 );
      break;

    case pointerdecl:
      mark_non_roots ( root->pointer.vartype, level + 1 );
      break;

    case typedecl:
      root->unk.root_entry = false;
      break;
  }
}

void mark_root_entries ()
{
  dcldesc * curEntry;

  for ( DLL_IterBegin(symboltable); 
	!DLL_IterAtEnd(symboltable); 
	DLL_IterNext(symboltable)    ) {

    curEntry = (dcldesc *) DLL_IterCurrent(symboltable);
    if ( curEntry->unk.root_entry )
      mark_non_roots ( curEntry, 0 );
  }
}


void calculate_sizes ()
{

}

/*
void assign_var_locations ()
{
  dcldesc * curEntry;
  dcldesc * funcEntry;
  dcldesc * formalEntry;
  dcldesc * localEntry;
  int cur_x;

  for ( DLL_IterBegin(symboltable); 
	!DLL_IterAtEnd(symboltable); 
	DLL_IterNext(symboltable)    ) {

    curEntry = (dcldesc *) DLL_IterCurrent(symboltable);

    if ( curEntry->unk.root_entry && curEntry->unk.type == vardecl ) {
      
      funcEntry = curEntry->var.vartype;
      if ( funcEntry->unk.type == funcdecl ) {

	cur_x = LOCAL_OFFSET;
	formalEntry = funcEntry->func.formallist;
	
	while ( formalEntry != NULL ) {
	  formalEntry->formal.offset = cur_x;
	  cur_x += calc_size ( formalEntry->formal.vartype );
	  
	  formalEntry = formalEntry->formal.formallink;
	}

	funcEntry->func.paramsize = cur_x - LOCAL_OFFSET;
      }
    }
  }
}
*/

void build_symbol_table ()
{
  symboltable = DLL_Create ();
  local_vars = DLL_Create ();

  insert_special_symtab_entries();

  cur_base_type = Stack_Create ();
  cur_declarator = NULL;
  cur_code_root = NULL;

  local_scope = false;

  build_symtab_from_tree ( syntaxtree );

  mark_root_entries ();

  calculate_sizes ();
}

