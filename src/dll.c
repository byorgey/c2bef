/* dll.c

   Doubly-linked list ADT implementation

   C-to-Befunge translation project

   (c) 2002 Steve Winslow and Brent Yorgey

   9/13/02
*/

#include "dll.h"


DLL DLL_Create ()
{
  DLL list = (DLL) malloc ( sizeof(DLL_struct) );
  
  list->num_elts = 0;
  list->head = list->tail = list->current = NULL;

  return list;
}


status_code DLL_Clear ( DLL list )
{
  //  if ( list == NULL ) return failure;

  DLL_node_ptr scan = list->head;
  DLL_node_ptr to_delete;
  Item_ptr item_to_delete;

  while ( scan != NULL ) {
    to_delete = scan;
    item_to_delete = scan->item;
    scan = scan->next;
    
    free(to_delete);
    free(item_to_delete);
  }

  list->head = list->tail = list->current = NULL;
  list->num_elts = 0;

  return OK;
}

status_code DLL_MakeEmpty ( DLL list )
{
  DLL_node_ptr scan = list->head;
  DLL_node_ptr to_delete;

  while ( scan != NULL ) {
    to_delete = scan;
    scan = scan->next;
    
    free(to_delete);
  }

  list->head = list->tail = list->current = NULL;
  list->num_elts = 0;

  return OK;
}  

status_code DLL_Destroy ( DLL list )
{
  status_code status = DLL_Clear ( list );
  if ( status == OK && list != NULL ) {
    free(list);
    return OK;
  } else {
    return failure;
  }
}


bool DLL_IsEmpty ( const DLL list )
{
  return (list->num_elts == 0);
}


long int DLL_Size ( const DLL list )
{
  return list->num_elts;
}


Item_ptr DLL_Front ( const DLL list )
{
  if ( DLL_IsEmpty(list) )
    return NULL;
  else
    return list->head->item;
}


Item_ptr DLL_Back ( const DLL list )
{
  if ( DLL_IsEmpty(list) )
    return NULL;
  else
    return list->tail->item;
}


Item_ptr DLL_ItemAt ( const DLL list, long int index )
{
  DLL_node_ptr scan;
  int i;

  if ( DLL_Size(list) <= index ) 
    return NULL;
  else {

    scan = list->head;
    for ( i = 0; i < index; i++ ) 
      scan = scan->next;

    return scan->item;
  }
}


Item_ptr DLL_Select ( const DLL list, Item_ptr target,
		      bool (*select) (Item_ptr item1, Item_ptr item2) )
{
  DLL_node_ptr scan = list->head;
   
  while ( scan != NULL && !(*select)(target, scan->item) )
    scan = scan->next;

  if ( scan == NULL ) 
    return NULL;
  else
    return scan->item;
}


DLL DLL_SelectAll ( const DLL list, Item_ptr target,
		    bool (*select) (Item_ptr item1, Item_ptr item2) )
{
  DLL result_list = DLL_Create();

  DLL_node_ptr scan = list->head;

  while ( scan != NULL ) {
    
    if ( (*select)(target, scan->item) ) 
      DLL_InsertBack(result_list, scan->item);

    scan = scan->next;
  }

  return result_list;
}

status_code DLL_InsertFront ( DLL list, Item_ptr newItem )
{
  //  if ( list == NULL ) return failure;

  DLL_node_ptr newNode = (DLL_node_ptr) malloc ( sizeof(DLL_node) );

  newNode->item = newItem;
  newNode->next = newNode->prev = NULL;

  if ( DLL_IsEmpty(list) ) {
    list->head = list->tail = newNode;
  } else {
    list->head->prev = newNode;
    newNode->next = list->head;
    list->head = newNode;
  }

  list->num_elts++;
  return OK;
}


status_code DLL_InsertBack ( DLL list, Item_ptr newItem )
{
  //  if ( list == NULL ) return failure;

  DLL_node_ptr newNode = (DLL_node_ptr) malloc ( sizeof(DLL_node) );

  newNode->item = newItem;
  newNode->next = newNode->prev = NULL;

  if ( DLL_IsEmpty(list) ) {
    list->head = list->tail = newNode;
  } else {
    list->tail->next = newNode;
    newNode->prev = list->tail;
    list->tail = newNode;
  }

  list->num_elts++;
  return OK;
}


status_code DLL_InsertAt ( DLL list, Item_ptr newItem, long int index )
{
  //  if ( list == NULL ) return failure;

  int len = DLL_Size(list);

  DLL_node_ptr newNode;
    
  DLL_node_ptr scan, previous;
  int i;

  if ( index > len || index < 0 ) 
    return NULL;
  else if ( index == 0 )
    return DLL_InsertFront ( list, newItem );
  else if ( index == len )
    return DLL_InsertBack ( list, newItem );
  else {

    newNode = (DLL_node_ptr) malloc ( sizeof(DLL_node) );
    newNode->item = newItem;

    scan = list->head;

    for ( i = 0; i < index; i++ )
      scan = scan->next;

    previous = scan->prev;

    previous->next = scan->prev = newNode;
    newNode->prev = previous;
    newNode->next = scan;

    list->num_elts++;
    return OK;
  }   
}


status_code DLL_DeleteFront ( DLL list )
{
  DLL_node_ptr toDelete;

  if ( DLL_IsEmpty(list) ) 
    return out_of_bounds;
  else if ( DLL_Size(list) == 1 ) {
    toDelete = list->head;
    list->head = list->tail = NULL;
  } else {
    toDelete = list->head;
    list->head = list->head->next;
    list->head->prev = NULL;
  }

  free(toDelete);
  list->num_elts--;

  return OK;
}


status_code DLL_DeleteBack ( DLL list )
{
  DLL_node_ptr toDelete;

  if ( DLL_IsEmpty(list) ) 
    return out_of_bounds;
  else if ( DLL_Size(list) == 1 ) {
    toDelete = list->head;
    list->head = list->tail = NULL;
  } else {
    toDelete = list->tail;
    list->tail = list->tail->prev;
    list->tail->next = NULL;
  }

  free(toDelete);
  list->num_elts--;

  return OK;
}


status_code DLL_DeleteAt ( DLL list, long int index )
{
  int len = DLL_Size(list);

  DLL_node_ptr toDelete, delPrev, delNext;
  int i;

  if ( index >= len || index < 0)
    return out_of_bounds;
  else if ( index == 0 )
    return DLL_DeleteFront ( list );
  else if ( index == (len - 1) )
    return DLL_DeleteBack ( list );
  else {
    // assert: DLL_Size(list) >= 3

    toDelete = list->head;

    for ( i = 0; i < index; i++ )
      toDelete = toDelete->next;

    delPrev = toDelete->prev;
    delNext = toDelete->next;

    delPrev->next = delNext;
    delNext->prev = delPrev;

    free(toDelete);
    list->num_elts--;

    return OK;
  }
}


// Never ever call this with node == NULL!!
void DLLh_DeleteNode ( DLL list, DLL_node_ptr node ) {

  if ( node == list->head )         // Redirect prev links (head or prev->next)
    list->head = node->next;
  else 
    node->prev->next = node->next;

  if ( node == list->tail )         // Redirect next links (tail or next->prev)
    list->tail = node->prev;
  else
    node->next->prev = node->prev;

  free(node);
  list->num_elts--;
}


status_code DLL_SearchAndDestroy ( DLL list, Item_ptr target,
				   bool (*items_equal) (Item_ptr item1, Item_ptr item2 ) )
{
  DLL_node_ptr scan = list->head;

  while ( scan != NULL && !(*items_equal)(target, scan->item) )
    scan = scan->next;

  if ( scan != NULL ) {
    DLLh_DeleteNode(list, scan);
    return OK;
  } else {
    return not_found;
  }
}


status_code DLL_SearchAndDestroyAll ( DLL list, Item_ptr target, 
				      bool (*items_equal) (Item_ptr item1, Item_ptr item2 ) )
{
  DLL_node_ptr scan = list->head;
  DLL_node_ptr toDelete;

  bool deletedOne = false;

  while ( scan != NULL ) {
    
    if ( (*items_equal)(target, scan->item) ) {
      toDelete = scan;
      scan = scan->next;
      DLLh_DeleteNode(list, toDelete);

      deletedOne = true;
    } else {
      scan = scan->next;
    }
  }

  if ( deletedOne )
    return OK;
  else
    return not_found;
}


void DLL_IterBegin ( DLL list )
{
  list->current = list->head;
}


bool DLL_IterAtEnd ( const DLL list )
{
  return ( list->current == NULL );
}


Item_ptr DLL_IterCurrent ( const DLL list )
{
  if ( DLL_IterAtEnd ( list ) )
    return NULL;
  else 
    return ( list->current->item );
}


void DLL_IterNext ( const DLL list )
{
  if ( !DLL_IterAtEnd ( list ) )
    list->current = list->current->next;
}


void DLL_Traverse ( DLL list, void (*ProcessItem) (Item_ptr item) )
{
  DLL_node_ptr scan = list->head;

  while (scan != NULL) {
    (*ProcessItem)(scan->item);
    scan = scan->next;
  }
}

void DLL_TraverseBackwards ( DLL list, void (*ProcessItem) (Item_ptr item) )
{
  DLL_node_ptr scan = list->tail;

  while (scan != NULL) {
    (*ProcessItem)(scan->item);
    scan = scan->prev;
  }
}


void DLL_ConcatLists ( DLL list1, DLL list2 )
{
  long int len1 = DLL_Size(list1);
  long int len2 = DLL_Size(list2);

  if ( len1 == 0 ) {
    list1->head = list2->head;          // list1 empty: just shove list2 into list1
    list1->tail = list2->tail;

  } else {

    list1->tail->next = list2->head;    // join list1's tail to list2's head
    if ( list2->head != NULL )
      list2->head->prev = list1->tail;

    list1->tail = list2->tail;
  }
  
  list2->head = list2->tail = NULL;    // clean up
  list1->num_elts += len2;
}


void DLL_ReverseList ( DLL list )
{
  // flip each node
  DLL_node_ptr scan = list->head, temp;

  while ( scan != NULL ) {
    temp = scan->next;
    scan->next = scan->prev;
    scan->prev = temp;

    scan = scan->prev;   // scan->prev is what scan->next used to be...
  }

  // now flip the whole list
  temp = list->head;
  list->head = list->tail;
  list->tail = temp;
}
